# EthicHub's subgraph

> Powered by [The Graph](https://thegraph.com)

:seedling: Lend affordable capital to unbanked farmers :seedling:

This subgraph dynamically tracks on-chain data from EthicHub's smart contracts.

## Hosted service

The subgraph is currently deployed to The Graph's [hosted service](https://thegraph.com/hosted-service).

- [EH Celo](https://thegraph.com/hosted-service/subgraph/ethichub/ethichub-celo)

- [EH Mainnet](https://thegraph.com/hosted-service/subgraph/ethichub/ethichub-mainnet)

- [EH Alfajores](https://thegraph.com/hosted-service/subgraph/ethichub/ethichub-alfajores)

## Entities and queries

All subgraph's entities can be queried through GraphQL endpoints.

You can find the `schema` with all available entities [here](https://gitlab.com/EthicHub/subgraph/-/blob/master/schema.graphql?ref_type=heads). 

Check the [querying](https://thegraph.com/docs/en/querying/querying-best-practices) section for more details.

| Network | GraphQL endpoint | Playground |
| --- | --- | --- |
| Celo | `https://api.thegraph.com/subgraphs/name/ethichub/ethichub-celo` | [API](https://api.thegraph.com/subgraphs/name/ethichub/ethichub-celo) |
| Mainnet | `https://api.thegraph.com/subgraphs/name/ethichub/ethichub-mainnet` | [API](https://api.thegraph.com/subgraphs/name/ethichub/ethichub-mainnet) |
| Alfajores | `https://api.thegraph.com/subgraphs/name/ethichub/ethichub-alfajores` | [API](https://api.thegraph.com/subgraphs/name/ethichub/ethichub-alfajores) |

## Development

### Prerequisites

1) Mappings are written in [AssemblyScript](https://github.com/AssemblyScript/assemblyscript/wiki), not TypeScript.

2) Read [The Graph's documentation](https://thegraph.com/docs/en/developing/creating-a-subgraph/).

3) Do not skip step 2.

### Setup

1) Use the latest `node` LTS version (or at least > v18.x).

2) Enable `corepack` in order to use `pnpm` as the package manager.

```bash
corepack enable
```

3) Install dependencies.

```bash
pnpm install
```

> Use `pnpm add` to add new dependencies. The `pnpm-lock.yaml` file is the source of truth for all installed dependencies.

4) Prepare the subgraph's manifest for the network you want to deploy to (check package.json).

```bash
pnpm run prepare:<network>
```

> **IMPORTANT:** Do not create or edit the `subgraph.yaml` file directly. Instead, use the `prepare` script along with the proper network yaml file located under the `networks` directory.

5) Autogenerate types for mappings.

```bash
pnpm run codegen
```

6) Write your custom mappings under the `src/mappings` directory.

7) Deploy to The Graph's hosted service.

```bash
pnpm run deploy:<network>
```

Notes:

- Use `pnpm run lint` before committing your changes to verify that the code style is correct.

- Before deploying to the hosted service, you need to authenticate yourself with your The Graph's account. 

- To get EH's account credentials, reach out to our team [here](https://t.me/ethichub/52419).

## License

[MIT](./LICENSE) License &copy; 2023-PRESENT [EthicHub](https://github.com/EthicHub)
