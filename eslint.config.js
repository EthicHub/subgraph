/* eslint perfectionist/sort-objects: 'error' */
// @ts-check
import { paroparo } from '@paro-paro/eslint-config'

export default paroparo(
  {
    gitignore: true,
    tsOptions: {
      tsconfigPath: './tsconfig.json',
    },
  },

  {
    files: ['src/mappings/**/*.ts', 'tests/**/*.ts'],
    languageOptions: {
      globals: {
        changetype: 'readonly',
      },
    },
    rules: {
      ...{
        'eqeqeq': 'off',
        'sort-exports/sort-exports': 'off',
        'unicorn/no-for-loop': 'off',
      },

      // recommended-type-checked
      // https://github.com/typescript-eslint/typescript-eslint/blob/main/packages/eslint-plugin/src/configs/recommended-type-checked.ts
      ...{
        'no-array-constructor': 'off',
        'no-implied-eval': 'off',
        'no-loss-of-precision': 'off',
        'no-unused-vars': 'off',
        'require-await': 'off',
        'ts/await-thenable': 'error',
        'ts/ban-ts-comment': 'error',
        'ts/ban-types': 'error',
        'ts/no-array-constructor': 'error',
        'ts/no-base-to-string': 'error',
        'ts/no-duplicate-enum-values': 'error',
        'ts/no-duplicate-type-constituents': 'error',
        'ts/no-explicit-any': 'error',
        'ts/no-extra-non-null-assertion': 'error',
        'ts/no-floating-promises': 'error',
        'ts/no-for-in-array': 'error',
        'ts/no-implied-eval': 'error',
        'ts/no-loss-of-precision': 'error',
        'ts/no-misused-new': 'error',
        'ts/no-misused-promises': 'error',
        'ts/no-namespace': 'error',
        'ts/no-non-null-asserted-optional-chain': 'error',
        'ts/no-redundant-type-constituents': 'error',
        'ts/no-this-alias': 'error',
        'ts/no-unnecessary-type-assertion': 'error',
        'ts/no-unnecessary-type-constraint': 'error',
        'ts/no-unsafe-argument': 'error',
        'ts/no-unsafe-assignment': 'error',
        'ts/no-unsafe-call': 'error',
        'ts/no-unsafe-declaration-merging': 'error',
        'ts/no-unsafe-enum-comparison': 'error',
        'ts/no-unsafe-member-access': 'error',
        'ts/no-unsafe-return': 'error',
        // 'ts/no-unused-vars': 'error',
        'ts/no-var-requires': 'error',
        'ts/prefer-as-const': 'error',
        'ts/require-await': 'error',
        'ts/restrict-plus-operands': 'error',
        'ts/restrict-template-expressions': 'error',
        'ts/triple-slash-reference': 'error',
        'ts/unbound-method': 'error',
      },

      // recommended-type-checked (overrides)
      ...{
        'ts/ban-types': ['error', { types: { BigInt: false } }],
        'ts/consistent-type-imports': 'off',
      },
    },
  },

  {
    files: ['tests/**/*.ts'],
    rules: {
      'unused-imports/no-unused-imports': 'off',
    },
  },

  // bug! => @paro-paro/eslint-config
  {
    files: ['**/*.yaml', '**/*.yml'],
    rules: {
      'stylistic/spaced-comment': 'off',
    },
  },
)
