specVersion: 1.0.0
description: Lend affordable capital to unbanked farmers
repository: https://gitlab.com/EthicHub/subgraph
schema:
  file: ./schema.graphql

dataSources:
  - name: Minimice
    kind: ethereum/contract
    network: {{network}}
    source:
      abi: Minimice
      address: '{{addressMinimice}}'
      startBlock: {{startBlockMinimice}}
    mapping:
      kind: ethereum/events
      apiVersion: 0.0.7
      language: wasm/assemblyscript
      entities:
        - Bond
        - BondHolder
        - BondContract
      abis:
        - name: Minimice
          file: ./abis/bond/Minimice.json
        - name: Membership
          file: ./abis/bond/Membership.json
        - name: ERC20
          file: ./abis/bond/ERC20.json
        - name: Legacy
          file: ./abis/bond/Legacy.json
      eventHandlers:
        - event: Transfer(indexed address,indexed address,indexed uint256)
          handler: handleTransfer
        - event: BondIssued(address,address,uint256,uint256,uint256,uint256,uint256,string,string)
          handler: handleBondIssued
        - event: BondRedeemed(address,address,uint256,uint256,uint256)
          handler: handleBondRedeemed
        - event: InterestParametersSet(uint256[],uint256[])
          handler: handleInterestParametersSet
        - event: MaxInterestParametersSet(uint256)
          handler: handleMaxInterestParametersSet
        - event: CooldownSet(uint256)
          handler: handleCooldownSet
        - event: CooldownStarted(uint256,uint256)
          handler: handleCooldownStarted
        - event: CollateralMultiplierUpdated(uint256)
          handler: handleCollateralMultiplierUpdated
        - event: CollateralExcessRemoved(indexed address)
          handler: handleCollateralExcessRemoved
        - event: CollateralAssigned(uint256,uint256)
          handler: handleCollateralAssigned
        - event: CollateralReleased(uint256,uint256)
          handler: handleCollateralReleased
        - event: LiquidityRequested(uint256,indexed address)
          handler: handleLiquidityRequested
        - event: LiquidityReturned(uint256,indexed address)
          handler: handleLiquidityReturned
      file: ./src/mappings/bond/minimice.ts

  - name: MinimiceEur
    kind: ethereum/contract
    network: {{network}}
    source:
      abi: Minimice
      address: '{{addressMinimiceEur}}'
      startBlock: {{startBlockMinimiceEur}}
    mapping:
      kind: ethereum/events
      apiVersion: 0.0.7
      language: wasm/assemblyscript
      entities:
        - Bond
        - BondHolder
        - BondContract
      abis:
        - name: Minimice
          file: ./abis/bond/Minimice.json
        - name: Membership
          file: ./abis/bond/Membership.json
        - name: ERC20
          file: ./abis/bond/ERC20.json
        - name: Legacy
          file: ./abis/bond/Legacy.json
      eventHandlers:
        - event: Transfer(indexed address,indexed address,indexed uint256)
          handler: handleTransfer
        - event: BondIssued(address,address,uint256,uint256,uint256,uint256,uint256,string,string)
          handler: handleBondIssued
        - event: BondRedeemed(address,address,uint256,uint256,uint256)
          handler: handleBondRedeemed
        - event: InterestParametersSet(uint256[],uint256[])
          handler: handleInterestParametersSet
        - event: MaxInterestParametersSet(uint256)
          handler: handleMaxInterestParametersSet
        - event: CooldownSet(uint256)
          handler: handleCooldownSet
        - event: CooldownStarted(uint256,uint256)
          handler: handleCooldownStarted
        - event: CollateralMultiplierUpdated(uint256)
          handler: handleCollateralMultiplierUpdated
        - event: CollateralExcessRemoved(indexed address)
          handler: handleCollateralExcessRemoved
        - event: CollateralAssigned(uint256,uint256)
          handler: handleCollateralAssigned
        - event: CollateralReleased(uint256,uint256)
          handler: handleCollateralReleased
        - event: LiquidityRequested(uint256,indexed address)
          handler: handleLiquidityRequested
        - event: LiquidityReturned(uint256,indexed address)
          handler: handleLiquidityReturned
      file: ./src/mappings/bond/minimice.ts

  - name: Membership
    kind: ethereum/contract
    network: {{network}}
    source:
      abi: Membership
      address: '{{addressMembership}}'
      startBlock: {{startBlockMembership}}
    mapping:
      kind: ethereum/events
      apiVersion: 0.0.7
      language: wasm/assemblyscript
      entities:
        - Bond
        - BondHolder
        - BondContract
      abis:
        - name: Minimice
          file: ./abis/bond/Minimice.json
        - name: Membership
          file: ./abis/bond/Membership.json
        - name: ERC20
          file: ./abis/bond/ERC20.json
        - name: Legacy
          file: ./abis/bond/Legacy.json
      eventHandlers:
        - event: Transfer(indexed address,indexed address,indexed uint256)
          handler: handleTransfer
        - event: BondIssued(address,address,uint256,uint256,uint256,uint256,uint256,string,string,string,int256)
          handler: handleBondIssued
        - event: BondRedeemed(address,address,uint256,uint256,uint256)
          handler: handleBondRedeemed
        - event: InterestParametersSet(uint256[],uint256[])
          handler: handleInterestParametersSet
        - event: MaxInterestParametersSet(uint256)
          handler: handleMaxInterestParametersSet
        - event: LiquidityRequested(uint256,indexed address)
          handler: handleLiquidityRequested
        - event: LiquidityReturned(uint256,indexed address)
          handler: handleLiquidityReturned
        - event: TypeOfBondSet(string)
          handler: handleTypeOfBondSet
        - event: TypeOfBondRemoved(string)
          handler: handleTypeOfBondRemoved
        - event: UpdatedBondType(uint256,string)
          handler: handleUpdatedBondType
        - event: UpdatedForeignId(uint256,int256)
          handler: handleUpdatedForeignId
      file: ./src/mappings/bond/membership.ts

  - name: StakeGeneral
    kind: ethereum/contract
    network: {{network}}
    source:
      abi: General
      address: '{{addressStakeGeneral}}'
      startBlock: {{startBlockStakeGeneral}}
    mapping:
      kind: ethereum/events
      apiVersion: 0.0.7
      language: wasm/assemblyscript
      entities:
        - Stake
        - StakeHolder
        - StakePool
      abis:
        - name: General
          file: ./abis/stake/General.json
        - name: FactoryOriginator
          file: ./abis/stake/FactoryOriginator.json
        - name: FactoryOriginatorLP
          file: ./abis/stake/FactoryOriginatorLP.json
        - name: Originator
          file: ./abis/stake/Originator.json
        - name: OriginatorLP
          file: ./abis/stake/OriginatorLP.json
        - name: OriginatorShared
          file: ./abis/stake/OriginatorShared.json
        - name: TransferABI
          file: ./abis/stake/TransferABI.json
      eventHandlers:
        - event: Transfer(indexed address,indexed address,uint256)
          handler: handleTransfer
        - event: Staked(indexed address,indexed address,uint256)
          handler: handleStaked
        - event: Redeem(indexed address,indexed address,uint256)
          handler: handleRedeem
        - event: AssetConfigUpdated(indexed address,uint256)
          handler: handleAssetConfigUpdated
        - event: RewardsClaimed(indexed address,indexed address,uint256)
          handler: handleRewardsClaimed
        - event: UnstakeWindowSet(uint256)
          handler: handleUnstakeWindowSet
        - event: CooldownSet(uint256)
          handler: handleCooldownSet
        - event: Cooldown(indexed address,uint256)
          handler: handleCooldown
      file: ./src/mappings/stake/general.ts

  - name: FactoryOriginator
    kind: ethereum/contract
    network: {{network}}
    source:
      abi: FactoryOriginator
      address: '{{addressFactoryStakeOriginator}}'
      startBlock: {{startBlockFactoryStakeOriginator}}
    mapping:
      kind: ethereum/events
      apiVersion: 0.0.7
      language: wasm/assemblyscript
      entities:
        - Stake
        - StakeHolder
        - StakePool
      abis:
        - name: General
          file: ./abis/stake/General.json
        - name: FactoryOriginator
          file: ./abis/stake/FactoryOriginator.json
        - name: FactoryOriginatorLP
          file: ./abis/stake/FactoryOriginatorLP.json
        - name: Originator
          file: ./abis/stake/Originator.json
        - name: OriginatorLP
          file: ./abis/stake/OriginatorLP.json
        - name: OriginatorShared
          file: ./abis/stake/OriginatorShared.json
        - name: TransferABI
          file: ./abis/stake/TransferABI.json
      eventHandlers:
        - event: OriginatorCreated(address,uint256)
          handler: handleStakeOriginatorCreated
        - event: OriginatorAdded(address,uint256)
          handler: handleStakeOriginatorAdded
      file: ./src/mappings/stake/factory.ts

  - name: FactoryOriginatorLP
    kind: ethereum/contract
    network: {{network}}
    source:
      abi: FactoryOriginatorLP
      address: '{{addressFactoryStakeOriginatorLP}}'
      startBlock: {{startBlockFactoryStakeOriginatorLP}}
    mapping:
      kind: ethereum/events
      apiVersion: 0.0.7
      language: wasm/assemblyscript
      entities:
        - Stake
        - StakeHolder
        - StakePool
      abis:
        - name: General
          file: ./abis/stake/General.json
        - name: FactoryOriginator
          file: ./abis/stake/FactoryOriginator.json
        - name: FactoryOriginatorLP
          file: ./abis/stake/FactoryOriginatorLP.json
        - name: Originator
          file: ./abis/stake/Originator.json
        - name: OriginatorLP
          file: ./abis/stake/OriginatorLP.json
        - name: OriginatorShared
          file: ./abis/stake/OriginatorShared.json
        - name: TransferABI
          file: ./abis/stake/TransferABI.json
      eventHandlers:
        - event: OriginatorCreated(address,uint256)
          handler: handleStakeOriginatorCreated_LP
        - event: OriginatorAdded(address,uint256)
          handler: handleStakeOriginatorAdded_LP
      file: ./src/mappings/stake/factory.ts

templates:
  - name: StakeOriginator
    kind: ethereum/contract
    network: {{network}}
    source:
      abi: OriginatorShared
    mapping:
      kind: ethereum/events
      apiVersion: 0.0.7
      language: wasm/assemblyscript
      entities:
        - Stake
        - StakeHolder
        - StakePool
      abis:
        - name: General
          file: ./abis/stake/General.json
        - name: FactoryOriginator
          file: ./abis/stake/FactoryOriginator.json
        - name: FactoryOriginatorLP
          file: ./abis/stake/FactoryOriginatorLP.json
        - name: Originator
          file: ./abis/stake/Originator.json
        - name: OriginatorLP
          file: ./abis/stake/OriginatorLP.json
        - name: OriginatorShared
          file: ./abis/stake/OriginatorShared.json
        - name: TransferABI
          file: ./abis/stake/TransferABI.json
      eventHandlers:
        - event: StateChange(uint256)
          handler: handleStateChange
        - event: Transfer(indexed address,indexed address,uint256)
          handler: handleTransfer
        - event: Staked(indexed address,indexed address,uint256)
          handler: handleStaked
        - event: Redeem(indexed address,indexed address,uint256)
          handler: handleRedeem
        - event: AssetConfigUpdated(indexed address,uint256)
          handler: handleAssetConfigUpdated
        - event: RewardsClaimed(indexed address,indexed address,uint256)
          handler: handleRewardsClaimed
        - event: DistributionEndChanged(uint256)
          handler: handleDistributionEndChanged
      file: ./src/mappings/stake/originator.ts

  - name: StakeOriginatorLP
    kind: ethereum/contract
    network: {{network}}
    source:
      abi: OriginatorShared
    mapping:
      kind: ethereum/events
      apiVersion: 0.0.7
      language: wasm/assemblyscript
      entities:
        - Stake
        - StakeHolder
        - StakePool
      abis:
        - name: General
          file: ./abis/stake/General.json
        - name: FactoryOriginator
          file: ./abis/stake/FactoryOriginator.json
        - name: FactoryOriginatorLP
          file: ./abis/stake/FactoryOriginatorLP.json
        - name: Originator
          file: ./abis/stake/Originator.json
        - name: OriginatorLP
          file: ./abis/stake/OriginatorLP.json
        - name: OriginatorShared
          file: ./abis/stake/OriginatorShared.json
        - name: TransferABI
          file: ./abis/stake/TransferABI.json
      eventHandlers:
        - event: StateChange(uint256)
          handler: handleStateChange_LP
        - event: Transfer(indexed address,indexed address,uint256)
          handler: handleTransfer
        - event: Staked(indexed address,indexed address,uint256)
          handler: handleStaked
        - event: Redeem(indexed address,indexed address,uint256)
          handler: handleRedeem
        - event: AssetConfigUpdated(indexed address,uint256)
          handler: handleAssetConfigUpdated
        - event: RewardsClaimed(indexed address,indexed address,uint256)
          handler: handleRewardsClaimed
        - event: DistributionEndChanged(uint256)
          handler: handleDistributionEndChanged
      file: ./src/mappings/stake/originator.ts
