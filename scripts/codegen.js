import fs from 'node:fs'
import path from 'node:path'
import process from 'node:process'

const src = path.join(process.cwd(), 'generated')
const target = path.join(process.cwd(), 'src/types')

copy()

/**
 * The purpose of this script is to copy the default graph-cli generated files (ABI's) to a custom `../src/types` folder,
 * grouping them in a more organized way so all imports from mappings files are way more readable (and maintainable).
 */
function copy() {
  const files = [
    ['schema.ts', 'schema.ts'],
    ['templates.ts', 'templates.ts'],
    ['Minimice', 'bond'],
    ['StakeGeneral', 'stake'],
  ]

  files.forEach((file) => {
    const srcPath = `${src}/${file[0]}`
    const targetPath = `${target}/${file[1]}`

    if (fs.existsSync(srcPath))
      fs.cpSync(srcPath, targetPath, { recursive: true }) // fs.cpSync is an experimental API
  })

  console.log(`Generated types succesfully copied to: ${target}`)
}
