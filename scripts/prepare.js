import fs from 'node:fs'
import path from 'node:path'
import process from 'node:process'
import { defu } from 'defu'
import yaml from 'js-yaml'
import mustache from 'mustache'

const network = process.argv[2]
const cwd = process.cwd()
const target = path.join(cwd, 'subgraph.yaml')

const json = path.join(cwd, `networks/${network}/json/${network}.json`)
const jsonLegacy = path.join(cwd, `networks/${network}/json/legacy.json`)

const src = path.join(cwd, `networks/${network}/${network}.yaml`)
const srcLegacy = path.join(cwd, `networks/${network}/legacy.yaml`)

prepare()

function prepare() {
  const jsonData = JSON.parse(fs.readFileSync(json))
  const jsonDataLegacy = JSON.parse(fs.readFileSync(jsonLegacy))

  const template = fs.readFileSync(src, 'utf8')
  const templateLegacy = fs.readFileSync(srcLegacy, 'utf8')

  const manifest = mustache.render(template, jsonData)
  const manifestLegacy = mustache.render(templateLegacy, jsonDataLegacy)

  const manifestContent = yaml.load(manifest)
  const manifestContentLegacy = yaml.load(manifestLegacy)

  const output = defu(manifestContent, manifestContentLegacy)
  fs.writeFileSync(target, yaml.dump(output), 'utf8')

  console.log(`Subgraph manifest generated (${network}): ${target}`)
}
