import { Address, BigInt, dataSource, store } from '@graphprotocol/graph-ts'
import {
  BondIssued as BondIssued_Legacy0,
  BondIssued1 as BondIssued_Legacy1,
  BondIssued2 as BondIssued_Legacy2,
  BondIssued3 as BondIssued_Legacy3,
  BondRedeemed as BondRedeemed_Legacy0,
  BondRedeemed1 as BondRedeemed_Legacy1,
  BondRedeemed2 as BondRedeemed_Legacy2,
} from '../../types/bond/Legacy'
import {
  BondIssued as BondIssued_Membership,
  BondRedeemed as BondRedeemed_Membership,
  InterestParametersSet as InterestParametersSet_Membership,
  LiquidityRequested as LiquidityRequested_Membership,
  LiquidityReturned as LiquidityReturned_Membership,
  MaxInterestParametersSet as MaxInterestParametersSet_Membership,
  Transfer as Transfer_Membership,
  TypeOfBondRemoved,
  TypeOfBondSet,
  UpdatedBondType,
  UpdatedForeignId,
} from '../../types/bond/Membership'
import {
  BondIssued as BondIssued_Minimice,
  BondRedeemed as BondRedeemed_Minimice,
  CollateralAssigned,
  CollateralExcessRemoved,
  CollateralMultiplierUpdated,
  CollateralReleased,
  CooldownSet,
  CooldownStarted,
  InterestParametersSet as InterestParametersSet_Minimice,
  LiquidityRequested as LiquidityRequested_Minimice,
  LiquidityReturned as LiquidityReturned_Minimice,
  MaxInterestParametersSet as MaxInterestParametersSet_Minimice,
  Transfer as Transfer_Minimice,
} from '../../types/bond/Minimice'
import { Bond, BondContract, BondHolder } from '../../types/schema'
import {
  ADDRESS_ZERO,
  BD_ZERO,
  BI_ZERO,
  FALLBACK_COOLDOWN,
  FALLBACK_COOLDOWN_MINIMICE,
  MAINNETS,
  MEMBERSHIP,
  MINIMICE,
  TOKEN_ERC20,
  getBondId,
  getContractERC20,
  getContractMembership,
  getContractMinimice,
  logger,
  toEther,
} from '../utils'
import {
  mapBondContractToAprs,
  mapBondContractToCurrency,
  mapBondContractToImages,
  mapBondContractToSizes,
  mapBondContractToType,
  mapBondContractsToBoosts,
  mapMaturityIdxToSize,
} from '../utils/mappers'

export function updateBondContract<T>(event: T): void {
  if (event instanceof InterestParametersSet_Minimice
    || event instanceof InterestParametersSet_Membership)
    updateBondContractParameters(event)

  else if (event instanceof MaxInterestParametersSet_Minimice
    || event instanceof MaxInterestParametersSet_Membership)
    updateBondContractParametersMax(event)

  else if (event instanceof LiquidityRequested_Minimice
    || event instanceof LiquidityRequested_Membership)
    updateBondContractPrincipalBalance(event)

  else if (event instanceof LiquidityReturned_Minimice
    || event instanceof LiquidityReturned_Membership)
    updateBondContractPrincipalBalance(event)

  else if (event instanceof TypeOfBondSet
    || event instanceof TypeOfBondRemoved)
    updateBondContractTypeOfBondsMembership(event)

  else if (event instanceof CooldownSet)
    updateBondContractCooldown(event)

  else if (event instanceof CollateralMultiplierUpdated)
    updateBondContractCollateralMultiplier(event)

  else if (event instanceof CollateralExcessRemoved)
    updateBondContractCollateralBalance(event)
}

export function bondTransferHelper<T>(event: T): void {
  if (event instanceof Transfer_Minimice || event instanceof Transfer_Membership) {
    const isMint = event.params.from == ADDRESS_ZERO // on buyBond
    const isBurn = event.params.to == ADDRESS_ZERO

    if (isBurn) { // is this possible?
      return
    }
    else if (isMint) {
      handleBondMint(event)
      return
    }

    // regular transfer: edge case
    logger('Transfer - BOND_ID: {} - FROM: {} - TO: {}', [
      event.params.tokenId.toString(),
      event.params.from.toHex(),
      event.params.to.toHex(),
    ])

    handleBondTransfer(event)
  }
}

/**
 * signatures:
 * BondIssued_Membership => buyer,beneficiary,tokenID,mintingDate,maturity,principal,interest,imageCID,bondTokenURI,typeOfBond,foreignId
 * BondIssued_Minimice   => buyer,beneficiary,tokenID,mintingDate,maturity,principal,interest,imageCID,bondTokenURI
 * BondIssued_Legacy0    => buyer,beneficiary,tokenID,mintingDate,maturity,principal,interest,imageCID,bondTokenURI
 * BondIssued_Legacy1    => buyer,tokenID,mintingDate,maturity,principal,interest,imageCID
 * BondIssued_Legacy2    => tokenID,mintingDate,maturity,principal,interest,imageCID
 * BondIssued_Legacy3    => tokenID,mintingDate,maturity,principal,interest
 */
export function bondIssuedHelper<T>(id: string, event: T): void {
  if (event instanceof BondIssued_Membership) {
    const bond = new Bond(id)
    const type = getBondType(event.address)
    bond.type = type
    bond.currency = getBondCurrency(event.address)
    bond.size = getBondSize(event.address, event.params.maturity, type)
    bond.principal = toEther(event.params.principal)
    bond.maturity = event.params.maturity
    bond.interest = event.params.interest
    bond.mintingDate = event.params.mintingDate
    bond.maturityDate = event.params.mintingDate.plus(event.params.maturity)
    bond.imageCID = event.params.imageCID
    bond.buyer = event.params.buyer
    bond.tokenURI = event.params.bondTokenURI
    bond.beneficiaryOnBuy = event.params.beneficiary
    bond.typeOfBondMembership = event.params.typeOfBond
    bond.foreignId = event.params.foreignId
    bond.save()
  }
  else if (event instanceof BondIssued_Minimice) {
    const bond = new Bond(id)
    const type = getBondType(event.address)
    bond.type = type
    bond.currency = getBondCurrency(event.address)
    bond.size = getBondSize(event.address, event.params.maturity, type)
    bond.principal = toEther(event.params.principal)
    bond.maturity = event.params.maturity
    bond.interest = event.params.interest
    bond.mintingDate = event.params.mintingDate
    bond.maturityDate = event.params.mintingDate.plus(event.params.maturity)
    bond.imageCID = event.params.imageCID
    bond.buyer = event.params.buyer
    bond.tokenURI = event.params.bondTokenURI
    bond.beneficiaryOnBuy = event.params.beneficiary
    bond.save()
  }
  else if (event instanceof BondIssued_Legacy0) {
    const bond = new Bond(id)
    const type = getBondType(event.address)
    bond.type = type
    bond.currency = getBondCurrency(event.address)
    bond.size = getBondSize(event.address, event.params.maturity, type)
    bond.principal = toEther(event.params.principal)
    bond.maturity = event.params.maturity
    bond.interest = event.params.interest
    bond.mintingDate = event.params.mintingDate
    bond.maturityDate = event.params.mintingDate.plus(event.params.maturity)
    bond.imageCID = event.params.imageCID
    bond.buyer = event.params.buyer
    bond.tokenURI = event.params.bondTokenURI
    bond.beneficiaryOnBuy = event.params.beneficiary
    bond.save()
  }
  else if (event instanceof BondIssued_Legacy1) {
    const bond = new Bond(id)
    const type = getBondType(event.address)
    bond.type = type
    bond.currency = getBondCurrency(event.address)
    bond.size = getBondSize(event.address, event.params.maturity, type)
    bond.principal = toEther(event.params.principal)
    bond.maturity = event.params.maturity
    bond.interest = event.params.interest
    bond.mintingDate = event.params.mintingDate
    bond.maturityDate = event.params.mintingDate.plus(event.params.maturity)
    bond.imageCID = event.params.imageCID
    bond.buyer = event.params.buyer
    bond.save()
  }
  else if (event instanceof BondIssued_Legacy2) {
    const bond = new Bond(id)
    const type = getBondType(event.address)
    bond.type = type
    bond.currency = getBondCurrency(event.address)
    bond.size = getBondSize(event.address, event.params.maturity, type)
    bond.principal = toEther(event.params.principal)
    bond.maturity = event.params.maturity
    bond.interest = event.params.interest
    bond.mintingDate = event.params.mintingDate
    bond.maturityDate = event.params.mintingDate.plus(event.params.maturity)
    bond.imageCID = event.params.imageCID
    bond.save()
  }
  else if (event instanceof BondIssued_Legacy3) {
    const bond = new Bond(id)
    const type = getBondType(event.address)
    bond.type = type
    bond.currency = getBondCurrency(event.address)
    bond.size = getBondSize(event.address, event.params.maturity, type)
    bond.principal = toEther(event.params.principal)
    bond.maturity = event.params.maturity
    bond.interest = event.params.interest
    bond.mintingDate = event.params.mintingDate
    bond.maturityDate = event.params.mintingDate.plus(event.params.maturity)
    bond.save()
  }
}

/**
 * signatures:
 * BondRedeemed         => redeemer,beneficiary,tokenID,redeemDate,withdrawn
 * BondRedeemed_Legacy0 => redeemer,tokenID,redeemDate,maturity,withdrawn,interest,imageCID
 * BondRedeemed_Legacy1 => tokenID,redeemDate,maturity,withdrawn,interest,imageCID
 * BondRedeemed_Legacy2 => tokenID,redeemDate,maturity,withdrawn,interest
 */
export function bondRedeemedHelper<T>(id: string, event: T): void {
  if (event instanceof BondRedeemed_Minimice || event instanceof BondRedeemed_Membership) {
    const bond = new Bond(id)
    bond.withdrawn = toEther(event.params.withdrawn)
    bond.redeemDate = event.params.redeemDate
    bond.redeemer = event.params.redeemer
    bond.beneficiaryOnRedeem = event.params.beneficiary
    bond.txHashRedeem = event.transaction.hash
    bond.save()
  }
  else if (event instanceof BondRedeemed_Legacy0) {
    const bond = new Bond(id)
    bond.withdrawn = toEther(event.params.withdrawn)
    bond.redeemDate = event.params.redeemDate
    bond.redeemer = event.params.redeemer
    bond.txHashRedeem = event.transaction.hash
    bond.save()
  }
  else if (event instanceof BondRedeemed_Legacy1) {
    const bond = new Bond(id)
    bond.withdrawn = toEther(event.params.withdrawn)
    bond.redeemDate = event.params.redeemDate
    bond.txHashRedeem = event.transaction.hash
    bond.save()
  }
  else if (event instanceof BondRedeemed_Legacy2) {
    const bond = new Bond(id)
    bond.withdrawn = toEther(event.params.withdrawn)
    bond.redeemDate = event.params.redeemDate
    bond.txHashRedeem = event.transaction.hash
    bond.save()
  }
}

export function bondCooldownHelper(id: string, event: CooldownStarted): void {
  const bond = new Bond(id)
  const cooldown = getCooldown(event.address)
  bond.cooldownStartDate = event.params.cooldown
  bond.cooldownEndDate = event.params.cooldown.plus(cooldown)
  bond.txHashCooldown = event.transaction.hash
  bond.save()
}

export function bondTypeHelper(id: string, event: UpdatedBondType): void {
  const bond = new Bond(id)
  bond.typeOfBondMembership = event.params.typeOfBond
  bond.save()
}

export function bondForeignIdHelper(id: string, event: UpdatedForeignId): void {
  const bond = new Bond(id)
  bond.foreignId = event.params.foreignId
  bond.save()
}

export function bondCollateralHelper<T>(id: string, event: T): void {
  if (event instanceof CollateralAssigned) {
    const bond = new Bond(id)
    bond.collateral = toEther(event.params.collateralAmount)
    bond.save()
  }
  else if (event instanceof CollateralReleased) {
    const bond = new Bond(id)
    bond.collateral = BD_ZERO
    bond.save()
  }
}

export function updateBondContractBalancePrincipalActive(address: Address, principal: BigInt): void {
  const ctr = BondContract.load(address)
  if (ctr === null)
    return

  const totalBalancePrincipalActive = ctr.totalBalancePrincipalActive
  if (totalBalancePrincipalActive === null)
    return

  ctr.totalBalancePrincipalActive = totalBalancePrincipalActive.plus(toEther(principal))
  ctr.save()
}

function initBondContractMinimice(event: InterestParametersSet_Minimice): void {
  const address = event.address
  const ctr = new BondContract(address)
  const minimice = getContractMinimice(address)
  ctr.type = MINIMICE
  ctr.currency = getBondCurrency(event.address)
  ctr.name = minimice.name()
  ctr.symbol = minimice.symbol()
  ctr.decimals = 0
  ctr.parametersLength = minimice.maxParametersLength()
  ctr.maturities = event.params.maturities
  ctr.interests = event.params.interests
  ctr.images = getBondContractImages(address)
  ctr.sizes = getBondContractSizes(address)
  ctr.aprs = getBondContractAprs(address)
  ctr.boosts = getBondContractBoosts(address)
  ctr.cooldown = getCooldown(address)
  ctr.totalSupply = BI_ZERO
  ctr.totalActive = BI_ZERO
  ctr.totalRedeemed = BI_ZERO
  ctr.principalTokenType = TOKEN_ERC20
  ctr.principalTokenAddress = minimice.principalTokenAddress()
  ctr.collateralTokenAddress = minimice.collateralTokenAddress()
  ctr.collateralMultiplier = minimice.collateralMultiplier()
  ctr.totalBalancePrincipal = BD_ZERO
  ctr.totalBalancePrincipalActive = BD_ZERO
  ctr.totalBalanceCollateral = BD_ZERO
  ctr.totalBalanceCollateralLocked = BD_ZERO
  ctr.createdAt = event.block.timestamp
  ctr.createdAtBlockNumber = event.block.number
  ctr.createdAtTxHash = event.transaction.hash
  ctr.save()
}

function initBondContractMembership(event: InterestParametersSet_Membership): void {
  const address = event.address
  const ctr = new BondContract(address)
  const membership = getContractMembership(address)
  ctr.type = MEMBERSHIP
  ctr.currency = getBondCurrency(address)
  ctr.name = membership.name()
  ctr.symbol = membership.symbol()
  ctr.decimals = 0
  ctr.parametersLength = membership.maxParametersLength()
  ctr.maturities = event.params.maturities
  ctr.interests = event.params.interests
  ctr.images = getBondContractImages(address)
  ctr.sizes = null
  ctr.aprs = null
  ctr.boosts = null
  ctr.cooldown = null
  ctr.totalSupply = BI_ZERO
  ctr.totalActive = BI_ZERO
  ctr.totalRedeemed = BI_ZERO
  ctr.principalTokenType = TOKEN_ERC20
  ctr.principalTokenAddress = membership.principalTokenAddress()
  ctr.collateralTokenAddress = null
  ctr.collateralMultiplier = null
  ctr.totalBalancePrincipal = BD_ZERO
  ctr.totalBalancePrincipalActive = null
  ctr.totalBalanceCollateral = null
  ctr.totalBalanceCollateralLocked = null
  ctr.createdAt = event.block.timestamp
  ctr.createdAtBlockNumber = event.block.number
  ctr.createdAtTxHash = event.transaction.hash
  ctr.save()
}

function updateBondContractParameters<T>(event: T): void {
  if (event instanceof InterestParametersSet_Minimice) {
    const ctr = BondContract.load(event.address)
    if (ctr === null) {
      initBondContractMinimice(event)
      return
    }
    ctr.maturities = event.params.maturities
    ctr.interests = event.params.interests
    ctr.save()
  }
  else if (event instanceof InterestParametersSet_Membership) {
    const ctr = BondContract.load(event.address)
    if (ctr === null) {
      initBondContractMembership(event)
      return
    }
    ctr.maturities = event.params.maturities
    ctr.interests = event.params.interests
    ctr.save()
  }
}

function updateBondContractParametersMax<T>(event: T): void {
  if (event instanceof MaxInterestParametersSet_Minimice || event instanceof MaxInterestParametersSet_Membership) {
    const ctr = new BondContract(event.address)
    ctr.parametersLength = event.params.value
    ctr.save()
  }
}

function updateBondContractPrincipalBalance<T>(event: T): void {
  if (event instanceof LiquidityRequested_Minimice || event instanceof LiquidityReturned_Minimice) {
    const minimice = getContractMinimice(event.address)
    const erc20 = getContractERC20(minimice.principalTokenAddress())
    const ctr = new BondContract(event.address)
    ctr.totalBalancePrincipal = toEther(erc20.balanceOf(event.address))
    ctr.save()
  }
  else if (event instanceof LiquidityRequested_Membership || event instanceof LiquidityReturned_Membership) {
    const membership = getContractMembership(event.address)
    const erc20 = getContractERC20(membership.principalTokenAddress())
    const ctr = new BondContract(event.address)
    ctr.totalBalancePrincipal = toEther(erc20.balanceOf(event.address))
    ctr.save()
  }
}

function updateBondContractTypeOfBondsMembership<T>(event: T): void {
  if (event instanceof TypeOfBondSet) {
    const typeOfBond = event.params.typeOfBond
    const ctr = BondContract.load(event.address)
    if (ctr === null)
      return

    let typeOfBondsMembership = ctr.typeOfBondsMembership
    if (typeOfBondsMembership === null)
      typeOfBondsMembership = new Array<string>()

    if (!typeOfBondsMembership.includes(typeOfBond)) {
      typeOfBondsMembership.push(typeOfBond)
      ctr.typeOfBondsMembership = typeOfBondsMembership
      ctr.save()
    }
  }
  else if (event instanceof TypeOfBondRemoved) {
    const typeOfBond = event.params.typeOfBond
    const ctr = BondContract.load(event.address)
    if (ctr === null)
      return

    let typeOfBondsMembership = ctr.typeOfBondsMembership
    if (typeOfBondsMembership === null)
      typeOfBondsMembership = new Array<string>()

    const arr = new Array<string>()
    for (let i = 0; i < typeOfBondsMembership.length; i++) {
      if (typeOfBondsMembership[i] !== typeOfBond)
        arr.push(typeOfBondsMembership[i])
    }
    ctr.typeOfBondsMembership = arr
    ctr.save()
  }
}

function updateBondContractCooldown(event: CooldownSet): void {
  const ctr = new BondContract(event.address)
  ctr.cooldown = event.params.cooldown
  ctr.save()
}

function updateBondContractCollateralMultiplier(event: CollateralMultiplierUpdated): void {
  const ctr = new BondContract(event.address)
  ctr.collateralMultiplier = event.params.collateralMultiplier
  ctr.save()
}

function updateBondContractCollateralBalance(event: CollateralExcessRemoved): void {
  const minimice = getContractMinimice(event.address)
  const erc20 = getContractERC20(minimice.collateralTokenAddress())
  const ctr = new BondContract(event.address)
  ctr.totalBalanceCollateral = toEther(erc20.balanceOf(event.address))
  ctr.save()
}

function handleBondMint<T>(event: T): void {
  if (event instanceof Transfer_Minimice || event instanceof Transfer_Membership) {
    const id = getBondId(event.params.tokenId, event.address)
    const bond = new Bond(id)
    const holder = new BondHolder(event.params.to)
    bond.tokenId = event.params.tokenId
    bond.holder = holder.id
    bond.txHashMint = event.transaction.hash
    holder.save()
    bond.save()
  }
}

function handleBondTransfer<T>(event: T): void {
  if (event instanceof Transfer_Minimice || event instanceof Transfer_Membership) {
    const id = getBondId(event.params.tokenId, event.address)
    const bond = new Bond(id)
    const holder = new BondHolder(event.params.to)
    bond.holder = holder.id
    holder.save()
    bond.save()
    handleBondTransferSender(event.params.from)
  }
}

function handleBondTransferSender(from: Address): void {
  const sender = BondHolder.load(from)
  if (sender === null)
    return

  const totalBonds = sender.bonds.load().length
  if (totalBonds == 1)
    store.remove('BondHolder', from.toHex())
}

function getCooldown(contractAddress: Address): BigInt {
  const contract = getContractMinimice(contractAddress)
  const call = contract.try_COOLDOWN()
  if (call.reverted) {
    return MAINNETS.includes(dataSource.network())
      ? FALLBACK_COOLDOWN_MINIMICE
      : FALLBACK_COOLDOWN
  }
  else {
    return call.value
  }
}

function getBondType(address: Address): string {
  const value = mapBondContractToType.get(address)
  return value === null ? 'error' : value
}

function getBondCurrency(address: Address): string {
  const value = mapBondContractToCurrency.get(address)
  return value === null ? 'error' : value
}

function getBondContractImages(address: Address): string[] {
  const value = mapBondContractToImages.get(address)
  return value === null ? [] : value
}

function getBondContractSizes(address: Address): string[] {
  const value = mapBondContractToSizes.get(address)
  return value === null ? [] : value
}

function getBondContractAprs(address: Address): string[] {
  const value = mapBondContractToAprs.get(address)
  return value === null ? [] : value
}

function getBondContractBoosts(address: Address): string[] {
  const value = mapBondContractsToBoosts.get(address)
  return value === null ? [] : value
}

function getBondSize(address: Address, maturity: BigInt, bondType: string): string | null {
  if (bondType == 'error')
    return 'error'

  if (bondType == MEMBERSHIP)
    return null

  const ctr = BondContract.load(address)
  if (ctr === null)
    return null

  const maturities = ctr.maturities.map<number>((x: BigInt) => x.toI32())
  const idx = getMaturityIdx(maturity.toI32(), maturities)
  return mapMaturityIdxToSize.get(idx)
}

export function getMaturityIdx(x: number, maturities: number[]): number {
  if (x <= maturities[0])
    return 0

  for (let i = 0; i < maturities.length - 1; i++) {
    if (x >= maturities[i] && x < maturities[i + 1])
      return i
  }

  return maturities.length - 1
}
