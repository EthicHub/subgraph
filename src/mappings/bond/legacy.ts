import { Address } from '@graphprotocol/graph-ts'
import {
  BondIssued as BondIssued_Legacy0,
  BondIssued1 as BondIssued_Legacy1,
  BondIssued2 as BondIssued_Legacy2,
  BondIssued3 as BondIssued_Legacy3,
  BondRedeemed as BondRedeemed_Legacy0,
  BondRedeemed1 as BondRedeemed_Legacy1,
  BondRedeemed2 as BondRedeemed_Legacy2,
} from '../../types/bond/Legacy'
import { BondContract } from '../../types/schema'
import { BI_ONE, getBondId, getContractERC20, getContractMembership, toEther } from '../utils'
import { bondIssuedHelper, bondRedeemedHelper, updateBondContractBalancePrincipalActive } from './helper'

// bond issued (legacy)
export function handleBondIssued_Legacy0_Membership(event: BondIssued_Legacy0): void {
  const id = getBondId(event.params.tokenId, event.address)
  bondIssuedHelper(id, event)
  updateBondContractBalances(event.address)
}

export function handleBondIssued_Legacy1(event: BondIssued_Legacy1): void {
  const id = getBondId(event.params.tokenId, event.address)
  bondIssuedHelper(id, event)
  updateBondContractBalancePrincipalActive(event.address, event.params.principal)
}

export function handleBondIssued_Legacy1_Membership(event: BondIssued_Legacy1): void {
  const id = getBondId(event.params.tokenId, event.address)
  bondIssuedHelper(id, event)
  updateBondContractBalances(event.address)
}

export function handleBondIssued_Legacy2(event: BondIssued_Legacy2): void {
  const id = getBondId(event.params.tokenId, event.address)
  bondIssuedHelper(id, event)
  updateBondContractBalancePrincipalActive(event.address, event.params.principal)
}

export function handleBondIssued_Legacy3(event: BondIssued_Legacy3): void {
  const id = getBondId(event.params.tokenId, event.address)
  bondIssuedHelper(id, event)
  updateBondContractBalancePrincipalActive(event.address, event.params.principal)
}

// bond redeemed (legacy)
export function handleBondRedeemed_Legacy0(event: BondRedeemed_Legacy0): void {
  const id = getBondId(event.params.tokenId, event.address)
  bondRedeemedHelper(id, event)
}

export function handleBondRedeemed_Legacy1(event: BondRedeemed_Legacy1): void {
  const id = getBondId(event.params.tokenId, event.address)
  bondRedeemedHelper(id, event)
}

export function handleBondRedeemed_Legacy2(event: BondRedeemed_Legacy2): void {
  const id = getBondId(event.params.tokenId, event.address)
  bondRedeemedHelper(id, event)
}

// helpers
function updateBondContractBalances(contractAddress: Address): void {
  const ctr = BondContract.load(contractAddress)
  if (ctr === null)
    return
  const membership = getContractMembership(contractAddress)
  const erc20_principal = getContractERC20(membership.principalTokenAddress())
  ctr.totalActive = ctr.totalActive.plus(BI_ONE)
  ctr.totalSupply = ctr.totalSupply.plus(BI_ONE)
  ctr.totalBalancePrincipal = toEther(erc20_principal.balanceOf(contractAddress))
  ctr.save()
}
