import {
  BondIssued,
  BondRedeemed,
  InterestParametersSet,
  LiquidityRequested,
  LiquidityReturned,
  MaxInterestParametersSet,
  Transfer,
  TypeOfBondRemoved,
  TypeOfBondSet,
  UpdatedBondType,
  UpdatedForeignId,
} from '../../types/bond/Membership'
import { BondContract } from '../../types/schema'
import { BI_ONE, getBondId, getContractERC20, getContractMembership, toEther } from '../utils'
import {
  bondForeignIdHelper,
  bondIssuedHelper,
  bondRedeemedHelper,
  bondTransferHelper,
  bondTypeHelper,
  updateBondContract,
} from './helper'

// events handlers
export function handleInterestParametersSet(event: InterestParametersSet): void {
  updateBondContract(event) // used as the Initialized event
}

export function handleMaxInterestParametersSet(event: MaxInterestParametersSet): void {
  updateBondContract(event)
}

export function handleLiquidityRequested(event: LiquidityRequested): void {
  updateBondContract(event)
}

export function handleLiquidityReturned(event: LiquidityReturned): void {
  updateBondContract(event)
}

export function handleTypeOfBondSet(event: TypeOfBondSet): void {
  updateBondContract(event)
}

export function handleTypeOfBondRemoved(event: TypeOfBondRemoved): void {
  updateBondContract(event)
}

export function handleTransfer(event: Transfer): void {
  bondTransferHelper(event)
}

export function handleBondIssued(event: BondIssued): void {
  const id = getBondId(event.params.tokenId, event.address)
  bondIssuedHelper(id, event)
  updateBondContractBalances(event)
}

export function handleBondRedeemed(event: BondRedeemed): void {
  const id = getBondId(event.params.tokenId, event.address)
  bondRedeemedHelper(id, event)
  updateBondContractBalances(event)
}

export function handleUpdatedBondType(event: UpdatedBondType): void {
  const id = getBondId(event.params.tokenId, event.address)
  bondTypeHelper(id, event)
}

export function handleUpdatedForeignId(event: UpdatedForeignId): void {
  const id = getBondId(event.params.tokenId, event.address)
  bondForeignIdHelper(id, event)
}

// helpers
function updateBondContractBalances<T>(event: T): void {
  if (event instanceof BondIssued || event instanceof BondRedeemed) {
    const ctr = BondContract.load(event.address)
    if (ctr === null)
      return

    if (event instanceof BondIssued) {
      ctr.totalActive = ctr.totalActive.plus(BI_ONE)
      ctr.totalSupply = ctr.totalSupply.plus(BI_ONE)
    }

    if (event instanceof BondRedeemed) {
      ctr.totalActive = ctr.totalActive.minus(BI_ONE)
      ctr.totalRedeemed = ctr.totalRedeemed.plus(BI_ONE)
    }

    const membership = getContractMembership(event.address)
    const erc20_principal = getContractERC20(membership.principalTokenAddress())
    ctr.totalBalancePrincipal = toEther(erc20_principal.balanceOf(event.address))
    ctr.save()
  }
}
