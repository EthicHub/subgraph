import {
  BondIssued,
  BondRedeemed,
  CollateralAssigned,
  CollateralExcessRemoved,
  CollateralMultiplierUpdated,
  CollateralReleased,
  CooldownSet,
  CooldownStarted,
  InterestParametersSet,
  LiquidityRequested,
  LiquidityReturned,
  MaxInterestParametersSet,
  Transfer,
} from '../../types/bond/Minimice'
import { Bond, BondContract } from '../../types/schema'
import { BI_ONE, getBondId, getContractERC20, getContractMinimice, toEther } from '../utils'
import {
  bondCollateralHelper,
  bondCooldownHelper,
  bondIssuedHelper,
  bondRedeemedHelper,
  bondTransferHelper,
  updateBondContract,
  updateBondContractBalancePrincipalActive,
} from './helper'

// events handlers
export function handleInterestParametersSet(event: InterestParametersSet): void {
  updateBondContract(event) // used as the Initialized event
}

export function handleMaxInterestParametersSet(event: MaxInterestParametersSet): void {
  updateBondContract(event)
}

export function handleCooldownSet(event: CooldownSet): void {
  updateBondContract(event)
}

export function handleCollateralMultiplierUpdated(event: CollateralMultiplierUpdated): void {
  updateBondContract(event)
}

export function handleCollateralExcessRemoved(event: CollateralExcessRemoved): void {
  updateBondContract(event)
}

export function handleLiquidityRequested(event: LiquidityRequested): void {
  updateBondContract(event)
}

export function handleLiquidityReturned(event: LiquidityReturned): void {
  updateBondContract(event)
}

export function handleTransfer(event: Transfer): void {
  bondTransferHelper(event)
}

export function handleBondIssued(event: BondIssued): void {
  const id = getBondId(event.params.tokenId, event.address)
  bondIssuedHelper(id, event)
  updateBondContractBalancePrincipalActive(event.address, event.params.principal)
}

export function handleBondRedeemed(event: BondRedeemed): void {
  const id = getBondId(event.params.tokenId, event.address)
  bondRedeemedHelper(id, event)
}

export function handleCooldownStarted(event: CooldownStarted): void {
  const id = getBondId(event.params.tokenId, event.address)
  bondCooldownHelper(id, event)
}

export function handleCollateralAssigned(event: CollateralAssigned): void {
  const id = getBondId(event.params.tokenId, event.address)
  bondCollateralHelper(id, event)
  updateBondContractBalances(event, id)
}

export function handleCollateralReleased(event: CollateralReleased): void {
  const id = getBondId(event.params.tokenId, event.address)
  bondCollateralHelper(id, event)
  updateBondContractBalances(event, id)
}

// helpers
function updateBondContractBalances<T>(event: T, bondId: string): void {
  if (event instanceof CollateralAssigned || event instanceof CollateralReleased) {
    const ctr = BondContract.load(event.address)
    if (ctr === null)
      return

    if (event instanceof CollateralAssigned) {
      ctr.totalActive = ctr.totalActive.plus(BI_ONE)
      ctr.totalSupply = ctr.totalSupply.plus(BI_ONE)
    }

    if (event instanceof CollateralReleased) {
      ctr.totalActive = ctr.totalActive.minus(BI_ONE)
      ctr.totalRedeemed = ctr.totalRedeemed.plus(BI_ONE)

      const totalBalancePrincipalActive = ctr.totalBalancePrincipalActive
      if (totalBalancePrincipalActive === null)
        return

      const bond = Bond.load(bondId)
      if (bond === null)
        return

      const principal = bond.principal
      if (principal === null)
        return

      ctr.totalBalancePrincipalActive = totalBalancePrincipalActive.minus(principal)
    }

    const minimice = getContractMinimice(event.address)
    const erc20_principal = getContractERC20(minimice.principalTokenAddress())
    const erc20_collateral = getContractERC20(minimice.collateralTokenAddress())
    ctr.totalBalancePrincipal = toEther(erc20_principal.balanceOf(event.address))
    ctr.totalBalanceCollateral = toEther(erc20_collateral.balanceOf(event.address))
    ctr.totalBalanceCollateralLocked = toEther(minimice.totalCollateralizedAmount())
    ctr.save()
  }
}
