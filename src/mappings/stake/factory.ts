import { OriginatorAdded, OriginatorCreated } from '../../types/stake/FactoryOriginator'
import { OriginatorAdded as OriginatorAdded_LP, OriginatorCreated as OriginatorCreated_LP } from '../../types/stake/FactoryOriginatorLP'
import { factoryContractHelper } from './helpers/originator'

// events handlers
export function handleStakeOriginatorCreated(event: OriginatorCreated): void {
  factoryContractHelper(event)
}

export function handleStakeOriginatorCreated_LP(event: OriginatorCreated_LP): void {
  factoryContractHelper(event)
}

export function handleStakeOriginatorAdded(event: OriginatorAdded): void {
  factoryContractHelper(event)
}

export function handleStakeOriginatorAdded_LP(event: OriginatorAdded_LP): void {
  factoryContractHelper(event)
}
