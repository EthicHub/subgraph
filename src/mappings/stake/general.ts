import { AssetConfigUpdated, Cooldown, CooldownSet, Redeem, RewardsClaimed, Staked, UnstakeWindowSet } from '../../types/stake/General'
import { Transfer } from '../../types/stake/TransferABI'
import { getStakeId } from '../utils'
import { stakeCooldownHelper, stakeRewardsClaimedHelper, updateStakePool } from './helpers/general'
import { stakeTransferHelper } from './helpers/transfer'

// events handlers
export function handleAssetConfigUpdated(event: AssetConfigUpdated): void {
  updateStakePool(event) // used as the Initialized event
}

export function handleCooldownSet(event: CooldownSet): void {
  updateStakePool(event)
}

export function handleUnstakeWindowSet(event: UnstakeWindowSet): void {
  updateStakePool(event)
}

export function handleStaked(event: Staked): void {
  updateStakePool(event)
}

export function handleRedeem(event: Redeem): void {
  updateStakePool(event)
}

export function handleTransfer(event: Transfer): void {
  stakeTransferHelper(event)
}

export function handleRewardsClaimed(event: RewardsClaimed): void {
  const id = getStakeId(event.address, event.params.to)
  stakeRewardsClaimedHelper(id, event)
}

export function handleCooldown(event: Cooldown): void {
  const id = getStakeId(event.address, event.params.user)
  stakeCooldownHelper(id, event)
}
