import { Address, BigInt } from '@graphprotocol/graph-ts'
import { Stake, StakePool } from '../../../types/schema'
import {
  AssetConfigUpdated,
  Cooldown,
  CooldownSet,
  Redeem,
  RewardsClaimed,
  Staked,
  UnstakeWindowSet,
} from '../../../types/stake/General'
import {
  STAKE_SUBTYPE_TRANSFER,
  STAKE_TYPE_GENERAL,
  getContractStakeGeneral,
  getEmissionsPerDay,
  toEther,
} from '../../utils'

export function updateStakePool<T>(event: T): void {
  if (event instanceof AssetConfigUpdated)
    updateStakePoolEmissions(event)

  else if (event instanceof CooldownSet)
    updateStakePoolCooldown(event)

  else if (event instanceof UnstakeWindowSet)
    updateStakePoolUnstakeWindow(event)

  else if (event instanceof Staked)
    updateStakePoolOnStaked(event)

  else if (event instanceof Redeem)
    updateStakePoolOnRedeem(event)
}

export function stakeRewardsClaimedHelper(id: string, event: RewardsClaimed): void {
  const stake = Stake.load(id)
  if (stake === null)
    return
  stake.totalClaimed = stake.totalClaimed.plus(toEther(event.params.amount))
  stake.save()
}

export function stakeCooldownHelper(id: string, event: Cooldown): void {
  const stake = new Stake(id)
  const stakeCooldown = getStakeCooldown(event.address)
  const stakeUnstakeWindow = getStakeUnstakeWindow(event.address)
  stake.cooldownStartDate = event.params.cooldown
  stake.cooldownEndDate = event.params.cooldown.plus(stakeCooldown) // this value is also the start date of the unstake window
  stake.unstakeWindowEndDate = event.params.cooldown.plus(stakeCooldown).plus(stakeUnstakeWindow)
  stake.save()
}

function initStakePoolGeneral(address: Address): void {
  const pool = new StakePool(address)
  const contract = getContractStakeGeneral(address)
  const emissionsPerSecond = contract.assets(address).value0
  const totalStaked = contract.totalSupply()
  pool.type = STAKE_TYPE_GENERAL
  pool.subtype = STAKE_SUBTYPE_TRANSFER
  pool.name = contract.name()
  pool.symbol = contract.symbol()
  pool.decimals = contract.decimals()
  pool.tokenToStake = contract.STAKED_TOKEN()
  pool.emissionsPerDay = getEmissionsPerDay(emissionsPerSecond)
  pool.cooldown = contract.COOLDOWN_SECONDS()
  pool.unstakeWindow = contract.UNSTAKE_WINDOW()
  pool.totalStaked = toEther(totalStaked)
  pool.save()
}

function updateStakePoolEmissions(event: AssetConfigUpdated): void {
  const pool = StakePool.load(event.address)
  if (pool === null) {
    initStakePoolGeneral(event.address)
    return
  }
  const emissionsPerSecond = event.params.emission
  pool.emissionsPerDay = getEmissionsPerDay(emissionsPerSecond)
  pool.save()
}

function updateStakePoolCooldown(event: CooldownSet): void {
  const pool = new StakePool(event.address)
  pool.cooldown = event.params.cooldown
  pool.save()
}

function updateStakePoolUnstakeWindow(event: UnstakeWindowSet): void {
  const pool = new StakePool(event.address)
  pool.unstakeWindow = event.params.unstakeWindow
  pool.save()
}

function updateStakePoolOnStaked(event: Staked): void {
  const pool = StakePool.load(event.address)
  if (pool === null)
    return
  const amount = toEther(event.params.amount)
  pool.totalStaked = pool.totalStaked.plus(amount)
  pool.save()
}

function updateStakePoolOnRedeem(event: Redeem): void {
  const pool = StakePool.load(event.address)
  if (pool === null)
    return
  const amount = toEther(event.params.amount)
  pool.totalStaked = pool.totalStaked.minus(amount)
  pool.save()
}

function getStakeCooldown(contractAddress: Address): BigInt {
  const contract = getContractStakeGeneral(contractAddress)
  return contract.COOLDOWN_SECONDS()
}

function getStakeUnstakeWindow(contractAddress: Address): BigInt {
  const contract = getContractStakeGeneral(contractAddress)
  return contract.UNSTAKE_WINDOW()
}
