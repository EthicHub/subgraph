import { Address } from '@graphprotocol/graph-ts'
import { Stake, StakePool } from '../../../types/schema'
import {
  OriginatorAdded,
  OriginatorCreated,
} from '../../../types/stake/FactoryOriginator'
import {
  OriginatorAdded as OriginatorAdded_LP,
  OriginatorCreated as OriginatorCreated_LP,
} from '../../../types/stake/FactoryOriginatorLP'
import {
  AssetConfigUpdated,
  DistributionEndChanged,
  Redeem,
  RewardsClaimed,
  Staked,
  StateChange,
} from '../../../types/stake/OriginatorShared'
import {
  StakeOriginator as StakeOriginatorTemplate,
  StakeOriginatorLP as StakeOriginatorTemplate_LP,
} from '../../../types/templates'
import {
  BD_ZERO,
  STAKE_ROLE_AUDITOR,
  STAKE_ROLE_ORIGINATOR,
  STAKE_SUBTYPE_LP,
  STAKE_SUBTYPE_TRANSFER,
  STAKE_TYPE_ORIGINATOR,
  getContractStakeOriginator,
  getContractStakeOriginatorLP,
  getEmissionsPerDay,
  toEther,
} from '../../utils'

export function factoryContractHelper<T>(event: T): void {
  if (event instanceof OriginatorCreated) {
    StakeOriginatorTemplate.create(event.params.originator)
  }
  else if (event instanceof OriginatorCreated_LP) {
    StakeOriginatorTemplate_LP.create(event.params.originator)
  }
  else if (event instanceof OriginatorAdded) {
    StakeOriginatorTemplate.create(event.params.originator)
    initStakePoolOriginator(event.params.originator, true)
  }
  else if (event instanceof OriginatorAdded_LP) {
    StakeOriginatorTemplate_LP.create(event.params.originator)
    initStakePoolOriginatorLP(event.params.originator, true)
  }
}

export function stateChangeHelper(event: StateChange, type: string): void {
  updateStakePoolState(event, type)
}

export function updateStakePool<T>(event: T): void {
  if (event instanceof AssetConfigUpdated)
    updateStakePoolEmissions(event)
  else if (event instanceof DistributionEndChanged)
    updateStakePoolDistributionEnd(event)
  else if (event instanceof Staked)
    updateStakePoolOnStaked(event)
  else if (event instanceof Redeem)
    updateStakePoolOnRedeem(event)
}

export function stakeRewardsClaimedHelper(id: string, event: RewardsClaimed): void {
  const stake = Stake.load(id)
  if (stake === null)
    return
  stake.totalClaimed = stake.totalClaimed.plus(toEther(event.params.amount))
  stake.save()
}

function initStakePoolOriginator(address: Address, isFactoryAdded: boolean): void {
  const pool = new StakePool(address)
  if (isFactoryAdded)
    pool.isFactoryAdded = isFactoryAdded
  const contract = getContractStakeOriginator(address)
  const emissionsPerSecond = contract.assets(address).value0
  const totalStakedGoal = contract.stakingGoal()
  const totalStakedAuditor = contract.proposerBalances(STAKE_ROLE_AUDITOR)
  const totalStakedOriginator = contract.proposerBalances(STAKE_ROLE_ORIGINATOR)
  const totalStakedCommunity = contract.totalSupply()
  const totalStakedCommunityGoal = totalStakedGoal.minus(totalStakedAuditor).minus(totalStakedOriginator)
  const totalStaked = totalStakedAuditor.plus(totalStakedOriginator).plus(totalStakedCommunity)
  pool.type = STAKE_TYPE_ORIGINATOR
  pool.subtype = STAKE_SUBTYPE_TRANSFER
  pool.name = contract.name()
  pool.symbol = contract.symbol()
  pool.decimals = contract.decimals()
  pool.tokenToStake = contract.STAKED_TOKEN()
  pool.emissionsPerDay = getEmissionsPerDay(emissionsPerSecond)
  pool.originatorState = contract.state()
  pool.originatorDefaultDate = contract.DEFAULT_DATE()
  pool.originatorDistributionEndDate = contract.DISTRIBUTION_END()
  pool.totalStakedGoal = toEther(totalStakedGoal)
  pool.totalStakedAuditor = toEther(totalStakedAuditor)
  pool.totalStakedOriginator = toEther(totalStakedOriginator)
  pool.totalStakedCommunity = toEther(totalStakedCommunity)
  pool.totalStakedCommunityGoal = toEther(totalStakedCommunityGoal)
  pool.totalStaked = toEther(totalStaked)
  pool.save()
}

function initStakePoolOriginatorLP(address: Address, isFactoryAdded: boolean): void {
  const pool = new StakePool(address)
  if (isFactoryAdded)
    pool.isFactoryAdded = isFactoryAdded
  const contract = getContractStakeOriginatorLP(address)
  const emissionsPerSecond = contract.assets(address).value0
  const totalStakedGoal = contract.stakingGoal()
  const totalStakedCommunity = contract.totalSupply()
  const totalStakedCommunityGoal = totalStakedGoal
  const totalStaked = totalStakedCommunity
  pool.type = STAKE_TYPE_ORIGINATOR
  pool.subtype = STAKE_SUBTYPE_LP
  pool.name = contract.name()
  pool.symbol = contract.symbol()
  pool.decimals = contract.decimals()
  pool.tokenToStake = contract.TOKEN_TO_STAKE()
  pool.emissionsPerDay = getEmissionsPerDay(emissionsPerSecond)
  pool.originatorState = contract.state()
  pool.originatorDefaultDate = contract.defaultDate()
  pool.originatorDistributionEndDate = contract.DISTRIBUTION_END()
  pool.totalStakedGoal = toEther(totalStakedGoal)
  pool.totalStakedAuditor = BD_ZERO
  pool.totalStakedOriginator = BD_ZERO
  pool.totalStakedCommunity = toEther(totalStakedCommunity)
  pool.totalStakedCommunityGoal = toEther(totalStakedCommunityGoal)
  pool.totalStaked = toEther(totalStaked)
  pool.save()
}

function updateStakePoolState(event: StateChange, type: string): void {
  const state = event.params.state.toI32()

  // uninitialized
  if (state == 0) {
    return
  }

  // stake open => setUpTerms()
  else if (state == 1 && type == STAKE_SUBTYPE_TRANSFER) {
    initStakePoolOriginator(event.address, false)
    return
  }

  else if (state == 1 && type == STAKE_SUBTYPE_LP) {
    initStakePoolOriginatorLP(event.address, false)
    return
  }

  const pool = new StakePool(event.address)
  pool.originatorState = state
  pool.save()
}

function updateStakePoolEmissions(event: AssetConfigUpdated): void {
  const pool = new StakePool(event.address)
  pool.emissionsPerDay = getEmissionsPerDay(event.params.emission)
  pool.save()
}

function updateStakePoolDistributionEnd(event: DistributionEndChanged): void {
  const pool = new StakePool(event.address)
  pool.originatorDistributionEndDate = event.params.distributionEnd
  pool.save()
}

function updateStakePoolOnStaked(event: Staked): void {
  const pool = StakePool.load(event.address)
  if (pool === null)
    return
  const amount = toEther(event.params.amount)
  const totalStakedCommunity = pool.totalStakedCommunity
  pool.totalStaked = pool.totalStaked.plus(amount)
  pool.totalStakedCommunity = totalStakedCommunity ? totalStakedCommunity.plus(amount) : null
  pool.save()
}

function updateStakePoolOnRedeem(event: Redeem): void {
  const pool = StakePool.load(event.address)
  if (pool === null)
    return
  const amount = toEther(event.params.amount)
  const totalStakedCommunity = pool.totalStakedCommunity
  pool.totalStaked = pool.totalStaked.minus(amount)
  pool.totalStakedCommunity = totalStakedCommunity ? totalStakedCommunity.minus(amount) : null
  pool.save()
}
