import { Stake, StakeHolder } from '../../../types/schema'
import { Transfer } from '../../../types/stake/TransferABI'
import { ADDRESS_ZERO, BD_ZERO, getStakeId, logger, toEther } from '../../utils'

export function stakeTransferHelper(event: Transfer): void {
  const isMint = event.params.from == ADDRESS_ZERO // on user stake
  const isBurn = event.params.to == ADDRESS_ZERO // on user redeem/unstake

  if (isMint) {
    handleStakeTransferRecipient(event)
    return
  }
  else if (isBurn) {
    handleStakeTransferSender(event)
    return
  }

  // regular transfer: edge case
  logger('Transfer - STK_TOKEN: {} - FROM: {} - TO: {} - AMOUNT: {}', [
    event.address.toHex(),
    event.params.from.toHex(),
    event.params.to.toHex(),
    toEther(event.params.value).toString(),
  ])

  handleStakeTransferRecipient(event)
  handleStakeTransferSender(event)
}

function handleStakeTransferRecipient(event: Transfer): void {
  const amount = toEther(event.params.value)
  const stakeId = getStakeId(event.address, event.params.to)
  let stake = Stake.load(stakeId)

  if (stake === null) {
    const holder = new StakeHolder(event.params.to)
    stake = new Stake(stakeId)
    stake.totalStaked = amount
    stake.totalClaimed = BD_ZERO
    stake.pool = event.address
    stake.holder = holder.id
    holder.save()
    stake.save()
    return
  }

  stake.totalStaked = stake.totalStaked.plus(amount)
  stake.save()
}

function handleStakeTransferSender(event: Transfer): void {
  const amount = toEther(event.params.value)
  const stakeId = getStakeId(event.address, event.params.from)
  const stake = Stake.load(stakeId)

  if (stake === null)
    return

  stake.totalStaked = stake.totalStaked.minus(amount)
  stake.save()
}
