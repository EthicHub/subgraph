import { StakePool } from '../../types/schema'
import { AssetConfigUpdated, DistributionEndChanged, Redeem, RewardsClaimed, Staked, StateChange } from '../../types/stake/OriginatorShared'
import { Transfer } from '../../types/stake/TransferABI'
import { STAKE_SUBTYPE_LP, STAKE_SUBTYPE_TRANSFER, getStakeId } from '../utils'
import { stakeRewardsClaimedHelper, stateChangeHelper, updateStakePool } from './helpers/originator'
import { stakeTransferHelper } from './helpers/transfer'

// events handlers
export function handleStateChange(event: StateChange): void {
  stateChangeHelper(event, STAKE_SUBTYPE_TRANSFER) // used as the Initialized event (since there is no Initialized event in the contract)
}

export function handleStateChange_LP(event: StateChange): void {
  stateChangeHelper(event, STAKE_SUBTYPE_LP)
}

export function handleAssetConfigUpdated(event: AssetConfigUpdated): void {
  updateStakePool(event)
}

export function handleDistributionEndChanged(event: DistributionEndChanged): void {
  updateStakePool(event)
}

export function handleStaked(event: Staked): void {
  updateStakePool(event)
}

export function handleRedeem(event: Redeem): void {
  updateStakePool(event)
}

export function handleTransfer(event: Transfer): void {
  const pool = StakePool.load(event.address)
  if (pool && !pool.isFactoryAdded)
    stakeTransferHelper(event)
}

export function handleRewardsClaimed(event: RewardsClaimed): void {
  const id = getStakeId(event.address, event.params.to)
  stakeRewardsClaimedHelper(id, event)
}
