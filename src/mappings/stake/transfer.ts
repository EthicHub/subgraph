import { Transfer } from '../../types/stake/TransferABI'
import { stakeTransferHelper } from './helpers/transfer'

// events handlers
export function handleTransfer(event: Transfer): void {
  stakeTransferHelper(event)
}
