import { Address, BigDecimal, BigInt, Bytes } from '@graphprotocol/graph-ts'

// networks
export const NETWORK_CELO = 'celo'
export const NETWORK_ETHEREUM = 'mainnet'
export const NETWORK_ALFAJORES = 'celo-alfajores'
export const TESTNETS = [NETWORK_ALFAJORES]
export const MAINNETS = [NETWORK_CELO, NETWORK_ETHEREUM]

// constants
export const MINIMICE = 'minimice'
export const MEMBERSHIP = 'membership'
export const TOKEN_ERC20 = 'erc20'
export const TOKEN_NATIVE = 'native'
export const CURRENCY_DAI = 'DAI'
export const CURRENCY_CUSD = 'cUSD'
export const CURRENCY_CEUR = 'cEUR'
export const CURRENCY_ETHIX = 'ETHIX'
export const BI_ZERO = BigInt.zero()
export const BI_ONE = BigInt.fromString('1')
export const BD_ZERO = BigDecimal.zero()
export const BD_ONE = BigDecimal.fromString('1')
export const BD_1E18 = BigDecimal.fromString('1000000000000000000') // 1e18
export const ADDRESS_ZERO = Address.zero()
export const FALLBACK_COOLDOWN = BigInt.fromI32(180) // 3 min
export const FALLBACK_COOLDOWN_STAKE = BigInt.fromI32(864000) // 10 days
export const FALLBACK_COOLDOWN_MINIMICE = BigInt.fromI32(432000) // 5 days
export const AUDITOR_HEX_STRING = '0x59a1c48e5837ad7a7f3dcedcbe129bf3249ec4fbf651fd4f5e2600ead39fe2f5'
export const ORIGINATOR_HEX_STRING = '0x59abfac6520ec36a6556b2a4dd949cc40007459bcd5cd2507f1e5cc77b6bc97e'

// stake
export const STAKE_TYPE_GENERAL = 'general'
export const STAKE_TYPE_ORIGINATOR = 'originator'
export const STAKE_SUBTYPE_LP = 'lp'
export const STAKE_SUBTYPE_TRANSFER = 'transfer'
export const STAKE_ROLE_AUDITOR = Bytes.fromHexString(AUDITOR_HEX_STRING)
export const STAKE_ROLE_ORIGINATOR = Bytes.fromHexString(ORIGINATOR_HEX_STRING)
