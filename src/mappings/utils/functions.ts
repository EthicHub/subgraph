import { Address, BigDecimal, BigInt, log } from '@graphprotocol/graph-ts'
import { ERC20 as ERC20Contract } from '../../types/bond/ERC20'
import { Membership as MembershipContract } from '../../types/bond/Membership'
import { Minimice as MinimiceContract } from '../../types/bond/Minimice'
import { General as StakeGeneralContract } from '../../types/stake/General'
import { Originator as StakeOriginatorContract } from '../../types/stake/Originator'
import { OriginatorLP as StakeOriginatorLPContract } from '../../types/stake/OriginatorLP'
import { BD_1E18 } from './constants'

export function toEther(value: BigInt): BigDecimal {
  return value.toBigDecimal().div(BD_1E18)
}

export function logger(message: string, values: string[]): void {
  log.warning(message, values)
}

export function getContractMinimice(address: Address): MinimiceContract {
  return MinimiceContract.bind(address)
}

export function getContractMembership(address: Address): MembershipContract {
  return MembershipContract.bind(address)
}

export function getContractStakeGeneral(address: Address): StakeGeneralContract {
  return StakeGeneralContract.bind(address)
}

export function getContractStakeOriginator(address: Address): StakeOriginatorContract {
  return StakeOriginatorContract.bind(address)
}

export function getContractStakeOriginatorLP(address: Address): StakeOriginatorLPContract {
  return StakeOriginatorLPContract.bind(address)
}

export function getContractERC20(address: Address): ERC20Contract {
  return ERC20Contract.bind(address)
}

export function getBondId(tokenId: BigInt, bondContractAddress: Address): string {
  return tokenId.toString().concat('-').concat(bondContractAddress.toHex())
}

export function getStakeId(stakeContractAddress: Address, stakerAddress: Address): string {
  return stakeContractAddress.toHex().concat('-').concat(stakerAddress.toHex())
}

export function getEmissionsPerDay(emissionsPerSecond: BigInt): BigDecimal {
  const secondsInDay = BigInt.fromI32(24 * 60 * 60)
  return toEther(emissionsPerSecond.times(secondsInDay))
}
