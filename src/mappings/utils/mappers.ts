import { Address, TypedMap } from '@graphprotocol/graph-ts'
import { CURRENCY_CEUR, CURRENCY_CUSD, CURRENCY_DAI, CURRENCY_ETHIX, MEMBERSHIP, MINIMICE } from './constants'

const BOND_MINIMICE_ETH_DAI = Address.fromString('0x21320683556BB718c8909080489F598120C554D9')
const BOND_MINIMICE_CELO_CUSD = Address.fromString('0x0f497a790429685a3CfD43b841865Ee185378ff0')
const BOND_MINIMICE_CELO_CEUR = Address.fromString('0xD8c31585639f044B32107e877518F30Efeb62c9B')
const BOND_MINIMICE_ALFAJORES_CUSD = Address.fromString('0x8CB7fC22c5F4CA9233cE8f03E9B431Dc6394C718')
const BOND_MINIMICE_ALFAJORES_CEUR = Address.fromString('0x8b9efBE6d9842144c1fAc2709c501E684a124077')
const BOND_MEMBERSHIP_CELO = Address.fromString('0x8903412ac24281421f1D94Fe27De56c0433f3d1f')
const BOND_MEMBERSHIP_ALFAJORES = Address.fromString('0x94b74513b044BC77aB2beC93cE965EB3274155E6')

const APRS_MINIMICE_DAI = ['6', '7', '9']
const APRS_MINIMICE_CUSD = ['6', '7', '8', '8']
const APRS_MINIMICE_CEUR = ['4', '5', '6', '7']

const BOOSTS_MINIMICE_DAI = ['0', '0', '0']
const BOOSTS_MINIMICE_CUSD = ['0', '0', '2', '5']
const BOOSTS_MINIMICE_CEUR = ['0', '0', '2', '5']

const SIZES_MINIMICE = ['small', 'medium', 'large', 'xlarge']
const SIZES_MINIMICE_ETH = ['small', 'medium', 'large']

const IMAGES_MINIMICE_DAI = [
  'QmWr9u1cD9hPkqM4k6HbpHBK17g6bfEKGZXeAFLX2N76K8', // +6% - 3 months
  'QmYpdz6ryQ6RCATgmGo67ddrAe458VKvwb6jfwAAN3hRvk', // +7% - 6 months
  'QmNdk9zoVAnJNfgWEZXGtpXT7sBWD59ZYsXKhaFWrmY67F', // +9% - 12 months
]

const IMAGES_MINIMICE_CUSD = [
  'QmWr9u1cD9hPkqM4k6HbpHBK17g6bfEKGZXeAFLX2N76K8', // +6% - 3 months
  'QmYpdz6ryQ6RCATgmGo67ddrAe458VKvwb6jfwAAN3hRvk', // +7% - 6 months
  'QmUBhmdFMx5KJ9zfAuQVJ1Ge1P66uHPhrpW8je2Dyozd1B', // +8% + 2% Ethix - 12 months
  'QmZsQCuXTkA2c5Wf1QDRJ2vpkQP8q7adbm2TLUamNevXvi', // +8% + 5% Ethix - 24 months
]

const IMAGES_MINIMICE_CEUR = [
  'QmNunvzXjuGRUEwuYtv23uhvF3oyXr81UoYVcPtSHCBgK7', // +4% - 3 months
  'QmZkYyXxJrn7dXmEbCfB34VFLXfieDToaRSBEH7qrzkXpi', // +5% - 6 months
  'Qma5KjDUAD6gR19kRwbE1UoVG3wnZNsZ4kuSXxwVWosC4X', // +6% + 2% - 12 months
  'QmZycL7joQDYVHPM2kevzo9D394SrwRmPWCaQYRtbsx2J7', // +7% + 5% - 24 months
]

const IMAGES_MEMBERSHIP = [
  'QmX4JeeJNyECHBk6RGx5M61wj561TdiiXM3E8iY3M7b6F8', // DEAL       => image: hand-shake => for email-form and minimice boost
  'QmZt8KuGXc7gc3doqBGhd94A9G3HPEwtePdha9uhgMdSKz', // AMBASSADOR => image: brush      => for influencers
  'QmSGSG6Ls19Bas5oY9SUrDV1JNDkjsS9Az5bJhYZy9wVKc', // AMBASSADOR => image: mic        => for strategists
  'QmZojeCjGvtuiftG9kv4sxCzADwgy1aCAuGsezNmT56H4m', // AMBASSADOR => image: earth      => for regional
  'QmSchNEAWrbZifBdc3N1roY2dbrb7E3d2kNCPNuS6h4Biz', // BOUNTY     => image: ethix logo => for bounties
]

const mapMaturityIdxToSize = new TypedMap<number, string>()
mapMaturityIdxToSize.set(0, SIZES_MINIMICE[0])
mapMaturityIdxToSize.set(1, SIZES_MINIMICE[1])
mapMaturityIdxToSize.set(2, SIZES_MINIMICE[2])
mapMaturityIdxToSize.set(3, SIZES_MINIMICE[3])

const mapBondContractToType = new TypedMap<Address, string>()
mapBondContractToType.set(BOND_MINIMICE_ETH_DAI, MINIMICE)
mapBondContractToType.set(BOND_MINIMICE_CELO_CUSD, MINIMICE)
mapBondContractToType.set(BOND_MINIMICE_CELO_CEUR, MINIMICE)
mapBondContractToType.set(BOND_MINIMICE_ALFAJORES_CUSD, MINIMICE)
mapBondContractToType.set(BOND_MINIMICE_ALFAJORES_CEUR, MINIMICE)
mapBondContractToType.set(BOND_MEMBERSHIP_CELO, MEMBERSHIP)
mapBondContractToType.set(BOND_MEMBERSHIP_ALFAJORES, MEMBERSHIP)

const mapBondContractToCurrency = new TypedMap<Address, string>()
mapBondContractToCurrency.set(BOND_MINIMICE_ETH_DAI, CURRENCY_DAI)
mapBondContractToCurrency.set(BOND_MINIMICE_CELO_CUSD, CURRENCY_CUSD)
mapBondContractToCurrency.set(BOND_MINIMICE_CELO_CEUR, CURRENCY_CEUR)
mapBondContractToCurrency.set(BOND_MINIMICE_ALFAJORES_CUSD, CURRENCY_CUSD)
mapBondContractToCurrency.set(BOND_MINIMICE_ALFAJORES_CEUR, CURRENCY_CEUR)
mapBondContractToCurrency.set(BOND_MEMBERSHIP_CELO, CURRENCY_ETHIX)
mapBondContractToCurrency.set(BOND_MEMBERSHIP_ALFAJORES, CURRENCY_ETHIX)

const mapBondContractToImages = new TypedMap<Address, string[]>()
mapBondContractToImages.set(BOND_MINIMICE_ETH_DAI, IMAGES_MINIMICE_DAI)
mapBondContractToImages.set(BOND_MINIMICE_CELO_CUSD, IMAGES_MINIMICE_CUSD)
mapBondContractToImages.set(BOND_MINIMICE_CELO_CEUR, IMAGES_MINIMICE_CEUR)
mapBondContractToImages.set(BOND_MINIMICE_ALFAJORES_CUSD, IMAGES_MINIMICE_CUSD)
mapBondContractToImages.set(BOND_MINIMICE_ALFAJORES_CEUR, IMAGES_MINIMICE_CEUR)
mapBondContractToImages.set(BOND_MEMBERSHIP_CELO, IMAGES_MEMBERSHIP)
mapBondContractToImages.set(BOND_MEMBERSHIP_ALFAJORES, IMAGES_MEMBERSHIP)

const mapBondContractToSizes = new TypedMap<Address, string[]>()
mapBondContractToSizes.set(BOND_MINIMICE_ETH_DAI, SIZES_MINIMICE_ETH)
mapBondContractToSizes.set(BOND_MINIMICE_CELO_CUSD, SIZES_MINIMICE)
mapBondContractToSizes.set(BOND_MINIMICE_CELO_CEUR, SIZES_MINIMICE)
mapBondContractToSizes.set(BOND_MINIMICE_ALFAJORES_CUSD, SIZES_MINIMICE)
mapBondContractToSizes.set(BOND_MINIMICE_ALFAJORES_CEUR, SIZES_MINIMICE)

const mapBondContractToAprs = new TypedMap<Address, string[]>()
mapBondContractToAprs.set(BOND_MINIMICE_ETH_DAI, APRS_MINIMICE_DAI)
mapBondContractToAprs.set(BOND_MINIMICE_CELO_CUSD, APRS_MINIMICE_CUSD)
mapBondContractToAprs.set(BOND_MINIMICE_CELO_CEUR, APRS_MINIMICE_CEUR)
mapBondContractToAprs.set(BOND_MINIMICE_ALFAJORES_CUSD, APRS_MINIMICE_CUSD)
mapBondContractToAprs.set(BOND_MINIMICE_ALFAJORES_CEUR, APRS_MINIMICE_CEUR)

const mapBondContractsToBoosts = new TypedMap<Address, string[]>()
mapBondContractsToBoosts.set(BOND_MINIMICE_ETH_DAI, BOOSTS_MINIMICE_DAI)
mapBondContractsToBoosts.set(BOND_MINIMICE_CELO_CUSD, BOOSTS_MINIMICE_CUSD)
mapBondContractsToBoosts.set(BOND_MINIMICE_CELO_CEUR, BOOSTS_MINIMICE_CEUR)
mapBondContractsToBoosts.set(BOND_MINIMICE_ALFAJORES_CUSD, BOOSTS_MINIMICE_CUSD)
mapBondContractsToBoosts.set(BOND_MINIMICE_ALFAJORES_CEUR, BOOSTS_MINIMICE_CEUR)

export {
  mapMaturityIdxToSize,
  mapBondContractToType,
  mapBondContractToCurrency,
  mapBondContractToImages,
  mapBondContractToAprs,
  mapBondContractsToBoosts,
  mapBondContractToSizes,
}
