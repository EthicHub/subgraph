import { Address } from '@graphprotocol/graph-ts'
import { afterAll, assert, beforeAll, clearStore, describe, test } from 'matchstick-as/assembly/index'
import { handleStakeOriginatorAdded, handleStakeOriginatorAdded_LP } from '../../../src/mappings/stake/factory'
import { STAKE_SUBTYPE_LP, STAKE_SUBTYPE_TRANSFER, STAKE_TYPE_ORIGINATOR } from '../../../src/mappings/utils'
import { OriginatorAdded } from '../../../src/types/stake/FactoryOriginator'
import { OriginatorAdded as OriginatorAdded_LP } from '../../../src/types/stake/FactoryOriginatorLP'
import { ADDRESS_ORIGINATOR_1, ADDRESS_ORIGINATOR_2, mockContractCallStakeOriginator, mockContractCallStakeOriginatorLP, mockEvent } from './utils'

const ENTITY_STAKE_POOL = 'StakePool'
const DATASOURCE_STAKE_ORIGINATOR = 'StakeOriginator'
const DATASOURCE_STAKE_ORIGINATOR_LP = 'StakeOriginatorLP'

const id1 = ADDRESS_ORIGINATOR_1.toLowerCase()
const id2 = ADDRESS_ORIGINATOR_2.toLowerCase()

describe('FactoryOriginator', () => {
  describe('On OriginatorAdded', () => {
    beforeAll(() => {
      const event = mockEvent<OriginatorAdded>(id1)
      mockContractCallStakeOriginator(id1)
      handleStakeOriginatorAdded(event)
    })

    afterAll(() => {
      clearStore()
    })

    test(`A new ${DATASOURCE_STAKE_ORIGINATOR} dataSource template is created.`, () => {
      assert.dataSourceCount(DATASOURCE_STAKE_ORIGINATOR, 1)
    })

    test(`The new ${DATASOURCE_STAKE_ORIGINATOR} dataSource template is created with the proper address.`, () => {
      assert.dataSourceExists(DATASOURCE_STAKE_ORIGINATOR, id1)
    })

    test(`The right ${ENTITY_STAKE_POOL} entity is saved in store.`, () => {
      assert.fieldEquals(ENTITY_STAKE_POOL, id1, 'id', id1)
      assert.fieldEquals(ENTITY_STAKE_POOL, id1, 'type', STAKE_TYPE_ORIGINATOR)
      assert.fieldEquals(ENTITY_STAKE_POOL, id1, 'subtype', STAKE_SUBTYPE_TRANSFER)
      assert.fieldEquals(ENTITY_STAKE_POOL, id1, 'isFactoryAdded', 'true')
    })

    test('Mocked contract calls in \'initStakePoolOriginator\' work as expected.', () => {
      assert.fieldEquals(ENTITY_STAKE_POOL, id1, 'name', 'foo')
      assert.fieldEquals(ENTITY_STAKE_POOL, id1, 'symbol', 'bar')
      assert.fieldEquals(ENTITY_STAKE_POOL, id1, 'decimals', '18')
      assert.fieldEquals(ENTITY_STAKE_POOL, id1, 'tokenToStake', Address.zero().toHex())
      assert.fieldEquals(ENTITY_STAKE_POOL, id1, 'emissionsPerDay', '0.0000000000000864') // 1 wei/sec * 86400 sec/day = 86400 wei/day
      assert.fieldEquals(ENTITY_STAKE_POOL, id1, 'originatorState', '1')
      assert.fieldEquals(ENTITY_STAKE_POOL, id1, 'originatorDefaultDate', '180')
      assert.fieldEquals(ENTITY_STAKE_POOL, id1, 'originatorDistributionEndDate', '180')
      assert.fieldEquals(ENTITY_STAKE_POOL, id1, 'totalStakedGoal', '0.00000000000000001') // 10 wei
      assert.fieldEquals(ENTITY_STAKE_POOL, id1, 'totalStakedAuditor', '0.000000000000000003') // 3 wei
      assert.fieldEquals(ENTITY_STAKE_POOL, id1, 'totalStakedOriginator', '0.000000000000000003') // 3 wei
      assert.fieldEquals(ENTITY_STAKE_POOL, id1, 'totalStakedCommunity', '0.000000000000000001') // 1 wei
      assert.fieldEquals(ENTITY_STAKE_POOL, id1, 'totalStakedCommunityGoal', '0.000000000000000004') // 10 - 3 - 3 = 4 wei
      assert.fieldEquals(ENTITY_STAKE_POOL, id1, 'totalStaked', '0.000000000000000007') // 3 + 3 + 1 = 7 wei
    })
  })
})

describe('FactoryOriginatorLP', () => {
  describe('On OriginatorAdded_LP', () => {
    beforeAll(() => {
      const event = mockEvent<OriginatorAdded_LP>(id2)
      mockContractCallStakeOriginatorLP(id2)
      handleStakeOriginatorAdded_LP(event)
    })

    afterAll(() => {
      clearStore()
    })

    test(`A new ${DATASOURCE_STAKE_ORIGINATOR_LP} dataSource template is created.`, () => {
      assert.dataSourceCount(DATASOURCE_STAKE_ORIGINATOR_LP, 1)
    })

    test(`The new ${DATASOURCE_STAKE_ORIGINATOR_LP} dataSource template is created with the proper address.`, () => {
      assert.dataSourceExists(DATASOURCE_STAKE_ORIGINATOR_LP, id2)
    })

    test(`The right ${ENTITY_STAKE_POOL} entity is saved in store.`, () => {
      assert.fieldEquals(ENTITY_STAKE_POOL, id2, 'id', id2)
      assert.fieldEquals(ENTITY_STAKE_POOL, id2, 'type', STAKE_TYPE_ORIGINATOR)
      assert.fieldEquals(ENTITY_STAKE_POOL, id2, 'subtype', STAKE_SUBTYPE_LP)
      assert.fieldEquals(ENTITY_STAKE_POOL, id2, 'isFactoryAdded', 'true')
    })

    test('Mocked contract calls in \'initStakePoolOriginatorLP\' work as expected.', () => {
      assert.fieldEquals(ENTITY_STAKE_POOL, id2, 'name', 'foo')
      assert.fieldEquals(ENTITY_STAKE_POOL, id2, 'symbol', 'bar')
      assert.fieldEquals(ENTITY_STAKE_POOL, id2, 'decimals', '18')
      assert.fieldEquals(ENTITY_STAKE_POOL, id2, 'tokenToStake', Address.zero().toHex())
      assert.fieldEquals(ENTITY_STAKE_POOL, id2, 'emissionsPerDay', '0.0000000000000864') // 1 wei/sec * 86400 sec/day = 86400 wei/day
      assert.fieldEquals(ENTITY_STAKE_POOL, id2, 'originatorState', '1')
      assert.fieldEquals(ENTITY_STAKE_POOL, id2, 'originatorDefaultDate', '180')
      assert.fieldEquals(ENTITY_STAKE_POOL, id2, 'originatorDistributionEndDate', '180')
      assert.fieldEquals(ENTITY_STAKE_POOL, id2, 'totalStakedGoal', '0.00000000000000001') // 10 wei
      assert.fieldEquals(ENTITY_STAKE_POOL, id2, 'totalStakedAuditor', '0')
      assert.fieldEquals(ENTITY_STAKE_POOL, id2, 'totalStakedOriginator', '0')
      assert.fieldEquals(ENTITY_STAKE_POOL, id2, 'totalStakedCommunity', '0.000000000000000001') // 1 wei
      assert.fieldEquals(ENTITY_STAKE_POOL, id2, 'totalStakedCommunityGoal', '0.00000000000000001') // 10 - 0 - 0 = 10 wei
      assert.fieldEquals(ENTITY_STAKE_POOL, id2, 'totalStaked', '0.000000000000000001') // 0 + 0 + 1 = 1 wei
    })
  })
})

// coverage
export {
  handleStakeOriginatorAdded,
  handleStakeOriginatorAdded_LP,
}
