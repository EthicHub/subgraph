import { assert, beforeAll, describe, test } from 'matchstick-as/assembly/index'
import { handleStakeOriginatorCreated, handleStakeOriginatorCreated_LP } from '../../../src/mappings/stake/factory'
import { OriginatorCreated } from '../../../src/types/stake/FactoryOriginator'
import { OriginatorCreated as OriginatorCreated_LP } from '../../../src/types/stake/FactoryOriginatorLP'
import { ADDRESS_ORIGINATOR_1, ADDRESS_ORIGINATOR_2, ADDRESS_ORIGINATOR_3, ADDRESS_ORIGINATOR_4, mockEvent } from './utils'

const ENTITY_STAKE_POOL = 'StakePool'
const DATASOURCE_STAKE_ORIGINATOR = 'StakeOriginator'
const DATASOURCE_STAKE_ORIGINATOR_LP = 'StakeOriginatorLP'

const id1 = ADDRESS_ORIGINATOR_1.toLowerCase()
const id2 = ADDRESS_ORIGINATOR_2.toLowerCase()
const id3 = ADDRESS_ORIGINATOR_3.toLowerCase()
const id4 = ADDRESS_ORIGINATOR_4.toLowerCase()

describe('FactoryOriginator', () => {
  describe('On OriginatorCreated', () => {
    beforeAll(() => {
      const event1 = mockEvent<OriginatorCreated>(id1)
      const event2 = mockEvent<OriginatorCreated>(id2)
      handleStakeOriginatorCreated(event1)
      handleStakeOriginatorCreated(event2)
    })

    test(`A new ${DATASOURCE_STAKE_ORIGINATOR} dataSource template is created.`, () => {
      assert.dataSourceCount(DATASOURCE_STAKE_ORIGINATOR, 2)
    })

    test(`The new ${DATASOURCE_STAKE_ORIGINATOR} dataSource template is created with the proper address.`, () => {
      assert.dataSourceExists(DATASOURCE_STAKE_ORIGINATOR, id1)
      assert.dataSourceExists(DATASOURCE_STAKE_ORIGINATOR, id2)
    })

    test(`No ${ENTITY_STAKE_POOL} entity is saved in store.`, () => {
      assert.notInStore(ENTITY_STAKE_POOL, id1)
      assert.notInStore(ENTITY_STAKE_POOL, id2)
    })
  })
})

describe('FactoryOriginatorLP', () => {
  describe('On OriginatorCreated_LP', () => {
    beforeAll(() => {
      const event1 = mockEvent<OriginatorCreated_LP>(id3)
      const event2 = mockEvent<OriginatorCreated_LP>(id4)
      handleStakeOriginatorCreated_LP(event1)
      handleStakeOriginatorCreated_LP(event2)
    })

    test(`A new ${DATASOURCE_STAKE_ORIGINATOR_LP} dataSource template is created.`, () => {
      assert.dataSourceCount(DATASOURCE_STAKE_ORIGINATOR_LP, 2)
    })

    test(`The new ${DATASOURCE_STAKE_ORIGINATOR_LP} dataSource template is created with the proper address.`, () => {
      assert.dataSourceExists(DATASOURCE_STAKE_ORIGINATOR_LP, id3)
      assert.dataSourceExists(DATASOURCE_STAKE_ORIGINATOR_LP, id4)
    })

    test(`No ${ENTITY_STAKE_POOL} entity is saved in store.`, () => {
      assert.notInStore(ENTITY_STAKE_POOL, id3)
      assert.notInStore(ENTITY_STAKE_POOL, id4)
    })
  })
})

// coverage
export {
  handleStakeOriginatorCreated,
  handleStakeOriginatorCreated_LP,
}
