import { Address, ethereum } from '@graphprotocol/graph-ts'
import { createMockedFunction, newMockEvent } from 'matchstick-as/assembly/index'
import { STAKE_ROLE_AUDITOR, STAKE_ROLE_ORIGINATOR } from '../../../src/mappings/utils'

export const VERBOSE = false
export const ADDRESS_ORIGINATOR_1 = '0x6541C4bbc52dC63B8e007625680979762d103e0F'
export const ADDRESS_ORIGINATOR_2 = '0xC9b83A66CA12f7FF0D11498EBB1fd42Ec2693C48'
export const ADDRESS_ORIGINATOR_3 = '0x06489476c1d22E8586056a727E58946439b2ECEc'
export const ADDRESS_ORIGINATOR_4 = '0xB42003475Bb4e2e65E237f3c0bb5cff0F7BbB5A0'

export function mockEvent<T>(originator: string): T {
  const mockEvent = newMockEvent()
  const originatorValue = ethereum.Value.fromAddress(Address.fromString(originator))
  const originatorParam = new ethereum.EventParam('originator', originatorValue)
  mockEvent.parameters.push(originatorParam)
  return changetype<T>(mockEvent)
}

export function mockContractCallStakeOriginator(contractAddress: string): void {
  const address = Address.fromString(contractAddress)
  mockCallName(address)
  mockCallSymbol(address)
  mockCallDecimals(address)
  mockCallStakedToken(address)
  mockCallState(address)
  mockCallDefaultDate(address)
  mockCallDistributionEnd(address)
  mockCallStakingGoal(address)
  mockCallTotalSupply(address)
  mockCallAssets(address)
  mockCallProposerBalancesAuditor(address)
  mockCallProposerBalancesOriginator(address)
}

export function mockContractCallStakeOriginatorLP(contractAddress: string): void {
  const address = Address.fromString(contractAddress)
  mockCallName(address)
  mockCallSymbol(address)
  mockCallDecimals(address)
  mockCallStakedTokenLP(address)
  mockCallState(address)
  mockCallDefaultDateLP(address)
  mockCallDistributionEnd(address)
  mockCallStakingGoal(address)
  mockCallTotalSupply(address)
  mockCallAssets(address)
}

export function mockCallName(contractAddress: Address): void {
  createMockedFunction(contractAddress, 'name', 'name():(string)')
    .returns([
      ethereum.Value.fromString('foo'),
    ])
}

export function mockCallSymbol(contractAddress: Address): void {
  createMockedFunction(contractAddress, 'symbol', 'symbol():(string)')
    .returns([
      ethereum.Value.fromString('bar'),
    ])
}

export function mockCallDecimals(contractAddress: Address): void {
  createMockedFunction(contractAddress, 'decimals', 'decimals():(uint8)')
    .returns([
      ethereum.Value.fromI32(18),
    ])
}

export function mockCallStakedToken(contractAddress: Address): void {
  createMockedFunction(contractAddress, 'STAKED_TOKEN', 'STAKED_TOKEN():(address)')
    .returns([
      ethereum.Value.fromAddress(Address.zero()),
    ])
}

export function mockCallStakedTokenLP(contractAddress: Address): void {
  createMockedFunction(contractAddress, 'TOKEN_TO_STAKE', 'TOKEN_TO_STAKE():(address)')
    .returns([
      ethereum.Value.fromAddress(Address.zero()),
    ])
}

export function mockCallDefaultDate(contractAddress: Address): void {
  createMockedFunction(contractAddress, 'DEFAULT_DATE', 'DEFAULT_DATE():(uint256)')
    .returns([
      ethereum.Value.fromI32(180),
    ])
}

export function mockCallDefaultDateLP(contractAddress: Address): void {
  createMockedFunction(contractAddress, 'defaultDate', 'defaultDate():(uint256)')
    .returns([
      ethereum.Value.fromI32(180),
    ])
}

export function mockCallDistributionEnd(contractAddress: Address): void {
  createMockedFunction(contractAddress, 'DISTRIBUTION_END', 'DISTRIBUTION_END():(uint256)')
    .returns([
      ethereum.Value.fromI32(180),
    ])
}

export function mockCallState(contractAddress: Address): void {
  createMockedFunction(contractAddress, 'state', 'state():(uint8)')
    .returns([
      ethereum.Value.fromI32(1),
    ])
}

export function mockCallStakingGoal(contractAddress: Address): void {
  createMockedFunction(contractAddress, 'stakingGoal', 'stakingGoal():(uint256)')
    .returns([
      ethereum.Value.fromI32(10), // wei
    ])
}

export function mockCallTotalSupply(contractAddress: Address): void {
  createMockedFunction(contractAddress, 'totalSupply', 'totalSupply():(uint256)')
    .returns([
      ethereum.Value.fromI32(1), // wei
    ])
}

export function mockCallAssets(contractAddress: Address): void {
  createMockedFunction(contractAddress, 'assets', 'assets(address):(uint128,uint128,uint256)')
    .withArgs([
      ethereum.Value.fromAddress(contractAddress),
    ])
    .returns([
      ethereum.Value.fromI32(1), // emissionPerSecond => 1 wei/sec
      ethereum.Value.fromI32(0), // lastUpdateTimestamp (not used)
      ethereum.Value.fromI32(0), // index (not used)
    ])
}

export function mockCallProposerBalancesAuditor(contractAddress: Address): void {
  createMockedFunction(contractAddress, 'proposerBalances', 'proposerBalances(bytes32):(uint256)')
    .withArgs([
      ethereum.Value.fromFixedBytes(STAKE_ROLE_AUDITOR),
    ])
    .returns([
      ethereum.Value.fromI32(3), // wei
    ])
}

export function mockCallProposerBalancesOriginator(contractAddress: Address): void {
  createMockedFunction(contractAddress, 'proposerBalances', 'proposerBalances(bytes32):(uint256)')
    .withArgs([
      ethereum.Value.fromFixedBytes(STAKE_ROLE_ORIGINATOR),
    ])
    .returns([
      ethereum.Value.fromI32(3), // wei
    ])
}
