import { BigInt } from '@graphprotocol/graph-ts'
import { assert, describe, test } from 'matchstick-as/assembly/index'
import { getMaturityIdx as getIdx } from '../../src/mappings/bond/helper'
import { getEmissionsPerDay, toEther } from '../../src/mappings/utils'
import { mapMaturityIdxToSize as mapper } from '../../src/mappings/utils/mappers'

const zero = BigInt.fromI32(0)
const oneWei = BigInt.fromString('1')
const oneInWei = BigInt.fromString('1000000000000000000')
const numberInWei = BigInt.fromString('214550345628182901029689')

// eps = emissions per second
const eps1 = BigInt.fromString('1250000000000000') // fabedi
const eps2 = BigInt.fromString('613425925925925') // sustenta
const eps3 = BigInt.fromString('2854166666666666') // sierra2
const eps4 = BigInt.fromString('570833333333333') // costal

const maturities = [
  BigInt.fromString('7776000'),
  BigInt.fromString('15552000'),
  BigInt.fromString('31536000'),
  BigInt.fromString('63072000'),
].map<number>((x: BigInt) => x.toI32())

describe('Utils functions', () => {
  test('toEther - Successfully converts from wei unit (in BigInt) to ether unit (in BigDecimal)', () => {
    assert.stringEquals(toEther(zero).toString(), '0')
    assert.stringEquals(toEther(oneInWei).toString(), '1')
    assert.stringEquals(toEther(oneWei).toString(), '0.000000000000000001')
    assert.stringEquals(toEther(numberInWei).toString(), '214550.345628182901029689')
  })

  test('getEmissionsPerDay - Successfully converts emissions per second to emissions per day', () => {
    assert.stringEquals(getEmissionsPerDay(zero).toString(), '0')
    assert.stringEquals(getEmissionsPerDay(oneWei).toString(), '0.0000000000000864') // 1 wei/sec * 86400 sec/day = 86400 wei/day = 0.0000000000000864 eth/day
    assert.stringEquals(getEmissionsPerDay(oneInWei).toString(), '86400') // 1 eth/sec * 86400 sec/day = 86400 eth/day
    assert.stringEquals(getEmissionsPerDay(eps1).toString(), '108')
    assert.stringEquals(getEmissionsPerDay(eps2).toString(), '52.99999999999992') // ~53.00
    assert.stringEquals(getEmissionsPerDay(eps3).toString(), '246.5999999999999424') // ~246.6
    assert.stringEquals(getEmissionsPerDay(eps4).toString(), '49.3199999999999712') // ~49.32
  })

  test('getBondSize (mapMaturityIdxToSize + getMaturityIdx) - Successfully maps bond maturity to custom bond size attribute', () => {
    assert.stringEquals('small', mapper.get(getIdx(0, maturities)) as string)
    assert.stringEquals('small', mapper.get(getIdx(500, maturities)) as string)
    assert.stringEquals('small', mapper.get(getIdx(7775999, maturities)) as string)
    assert.stringEquals('small', mapper.get(getIdx(7776000, maturities)) as string)
    assert.stringEquals('small', mapper.get(getIdx(7776001, maturities)) as string)
    assert.stringEquals('small', mapper.get(getIdx(15551999, maturities)) as string)
    assert.stringEquals('medium', mapper.get(getIdx(15552000, maturities)) as string)
    assert.stringEquals('medium', mapper.get(getIdx(15552001, maturities)) as string)
    assert.stringEquals('medium', mapper.get(getIdx(31535999, maturities)) as string)
    assert.stringEquals('large', mapper.get(getIdx(31536000, maturities)) as string)
    assert.stringEquals('large', mapper.get(getIdx(31536001, maturities)) as string)
    assert.stringEquals('large', mapper.get(getIdx(63071999, maturities)) as string)
    assert.stringEquals('xlarge', mapper.get(getIdx(63072000, maturities)) as string)
    assert.stringEquals('xlarge', mapper.get(getIdx(63072001, maturities)) as string)
    assert.stringEquals('xlarge', mapper.get(getIdx(100000000, maturities)) as string)
  })
})
